package com.example.tasklist;
import java.util.List;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.ListView;
import android.widget.Toast;
//import android.view.View.OnClickListener;

public class AdapterToTaskChooser extends BaseAdapter {
	public List<Task> listTask;
	public  boolean[] mCheckedState;
	private final Context context;
	public SharedPreferences pref;
	public int N;
	int index=0;
	public int zaznaczone;
	public SharedPreferences tempPref;
	public boolean isAfterRotate;
	
	//private boolean isAdapter =true;
	
	public AdapterToTaskChooser(List<Task> a, Context context,SharedPreferences pref,boolean isAfterRotate,boolean[] checked,int z) {
		listTask = a;
		this.pref=pref;
		this.context = context;
		N=pref.getInt("Nzad", 0);
		//zaznaczone=Math.min(countSelected(isAfterRotate),N);
		zaznaczone=z;
		mCheckedState = new boolean[listTask.size()];
		mCheckedState = checked;
		tempPref=context.getSharedPreferences("TEMP", Context.MODE_PRIVATE);
		this.isAfterRotate=tempPref.contains("isSelected"+String.valueOf(0));
		String test="";
		int x=0;
		for(int i=0;i<listTask.size();i++){
			test+=i+" " + mCheckedState[i]+"\n";
			if(mCheckedState[i])x++; 
		}
		zaznaczone=x;
		//Toast.makeText(context, test, Toast.LENGTH_LONG).show();
	}

	@Override
	public int getCount() {
		// TODO Auto-generated method stub
		return listTask.size();
	}

	@Override
	public Task getItem(int arg0) {
		// TODO Auto-generated method stub
		return listTask.get(arg0);
	}

	@Override
	public long getItemId(int arg0) {
		// TODO Auto-generated method stub
		return arg0;
	}
	public boolean[] getSelected(){
		return mCheckedState;
	}
	

	public int countSelected(){
		return zaznaczone;
	}
	public int getPosition(Task arg) {
		return listTask.indexOf(arg);
	}

	 static class ViewHolder {
	        protected Button button;
	        protected CheckBox checkbox;
	        protected int truePosition;
	    }
	 private int countSelected(boolean isAfterRotate){
		 int result=0;
		 if(isAfterRotate){
			 for(int i=0;i<listTask.size();i++)
				 if(tempPref.getBoolean("isSelected"+String.valueOf(i), false))result++;
			 }else{
				 for(int i=0;i<listTask.size();i++)
				 if(listTask.get(i).isSelected)result++;
			 }
		 return result;
	 }
	 
	@Override
	public View getView(final int position, View convertView, final ViewGroup parent) { // int
																		// position,
																		// View
																		// convertView,
																		// ViewGroup
																		// parent
		
		
		  ViewHolder viewHolder = null;
	        if (convertView == null) {
	        	LayoutInflater inflator = (LayoutInflater) context
	    				.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
	            convertView = inflator.inflate(R.layout.row_task_chooser, parent,false);
	           
	            viewHolder = new ViewHolder();
	            viewHolder.truePosition=index++;
	            final int getPosition = viewHolder.truePosition;
	           // viewHolder.text = (TextView) convertView.findViewById(R.id.label);
	            viewHolder.checkbox = (CheckBox) convertView.findViewById(R.id.checkBox1);
	            viewHolder.button = (Button) convertView.findViewById(R.id.szczegoly);
	            
	            viewHolder.button.setOnClickListener(new OnClickListener() {
					
					@Override
					public void onClick(View v) {
						Intent i = new Intent(context,TaskDetail.class);
						i.putExtra("id", listTask.get(position).id);
						i.putExtra("FROM", "TaskChooser");
						context.startActivity(i);
						
					}
				});
	            // final int getPosition;
	            viewHolder.checkbox.setEnabled(true);
	            viewHolder.checkbox.setChecked(mCheckedState[position]);
	           /* viewHolder.checkbox.setOnClickListener(new OnClickListener() {
					
					@Override
					public void onClick(View v) {
						// TODO Auto-generated method stub
						
					}
				});*/
	          //  final int truePosition = (Integer)viewHolder.checkbox.getTag();
	            viewHolder.checkbox.setOnClickListener(new OnClickListener() {
					
					@Override
					public void onClick(View v) {
						CheckBox cb = (CheckBox) v;
						
						boolean isChecked=cb.isChecked();
						if(isChecked){//jesli wcisniety
							//Toast.makeText(context, " "+zaznaczone, Toast.LENGTH_LONG).show();
							zaznaczone++;
							if(zaznaczone <= N ){
								//zaznaczone++;
								//Toast.makeText(context, "zmieniam pozycje:  "+position + "a prawdziwa to: "+getPosition, Toast.LENGTH_LONG).show();
								listTask.get(getPosition).isSelected=true;
								mCheckedState[getPosition]=true;
								if(zaznaczone == N || zaznaczone==listTask.size())
								show("Wybra�e� ustalon� liczb� zada�. Czy chcesz zaakceptowa�?", (TaskChooser)context);
							}else{
								zaznaczone--;
								cb.setChecked(false);
								show("NIE MO�ESZ JU� WYBRA�! \nWybra�e� ju� ustalona liczb� zada� czy chcesz je zaakceptowa�?",(TaskChooser)context);
							}
						}else{
							zaznaczone--;
							listTask.get(position).isSelected=false;
							mCheckedState[position]=false;
						}
						
					}
				});
	           /* viewHolder.checkbox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
	            	
	                @Override
	                public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
	                   // int getPosition = (Integer) buttonView.getTag();  // Here we get the position that we have set for the checkbox using setTag.
	                   // listTask.get(getPosition).isSelected=buttonView.isChecked(); // Set the value of checkbox to maintain its state.
	                	int getPosition =(Integer) buttonView.getTag();
	                	if(isChecked){
							if(zaznaczone==N){//za duzo wcisnietych
								buttonView.setChecked(false);
								listTask.get(getPosition).isSelected=false;
								// tempPref.edit().putBoolean("isSelected"+String.valueOf(getPosition), false).commit();
								show("NIE MO�ESZ JU� WYBRA�! \0Wybra�e� ju� ustalona liczb� zada� czy chcesz je zaakceptowa�?",(TaskChooser)context);
								//Toast.makeText(context, "mozesz zaznaczy� tylko " + N, Toast.LENGTH_LONG).show();
							}else{
								buttonView.setChecked(true);
								listTask.get(getPosition).isSelected=true;
								zaznaczone++;
								mCheckedState[position]=true;
								//mCheckedState[position]=true;
								//tempPref.edit().putBoolean("isSelected"+String.valueOf(position), true).commit();
								if(isAfterRotate)
								if(zaznaczone==N)show("Wybra�e� ustalon� liczb� zada�. Czy chcesz zaakceptowa�?", (TaskChooser)context);
								// tempPref.edit().putBoolean("isSelected"+String.valueOf(getPosition), true).commit();
							}
							
						}else{
							listTask.get(getPosition).isSelected=false;
							buttonView.setChecked(false);
							mCheckedState[position]=false;
							//mCheckedState[position]=false;
							//tempPref.edit().putBoolean("isSelected"+String.valueOf(position), false).commit();
							zaznaczone--;
							// tempPref.edit().putBoolean("isSelected"+String.valueOf(getPosition), false).commit();
						}
	                   
	                   // mCheckedState[position]=; // Set the value of checkbox to maintain its state.
	                    //pref.edit().putBoolean(String.valueOf(listTask.get(getPosition))+"isSelected", buttonView.isChecked()).commit();
	                    
	                }
	            });*/
	            //viewHolder.checkbox.setChecked(true);//pref.getBoolean(String.valueOf(listTask.get(position))+"isSelected", false));
	            
	           
	            convertView.setTag(viewHolder);
	            //convertView.setTag(R.id.label, viewHolder.text);
	            convertView.setTag(R.id.checkBox1, viewHolder.checkbox);
	            } else {
	            viewHolder = (ViewHolder) convertView.getTag();
	            viewHolder.checkbox.setChecked(mCheckedState[position]);
	        }
	        viewHolder.checkbox.setTag(position); // This line is important.
	        String text = listTask.get(position).getTitle() +" " + String.valueOf(listTask.get(position).isSelected);
	        text+= " pozycja w list task: " +String.valueOf(position);// + " pozycja inna: "+ String.valueOf(getPosition);
            viewHolder.checkbox.setText(text);
	        
            
	       /* if(isAfterRotate)
	        	viewHolder.checkbox.setChecked(tempPref.getBoolean("isSelected"+String.valueOf(position), false));
	        else*/
	        	//viewHolder.checkbox.setChecked(listTask.get(position).isSelected);
	        
	       
	        //viewHolder.checkbox.setChecked(true);
	        //viewHolder.text.setText(listTask.get(position).getName());
	       
	        return convertView;
	    }
	
	void show(String tekst, final Activity taskChooser){
		 AlertDialog.Builder alertDialog = new AlertDialog.Builder(context);


		// Setting Dialog Title
         alertDialog.setTitle("UWAGA");

         // Setting Dialog Message
         alertDialog.setMessage(tekst);

         // Setting Icon to Dialog
         alertDialog.setIcon(R.drawable.ic_launcher);

         // Setting Positive "Yes" Button
         alertDialog.setPositiveButton("YES", new DialogInterface.OnClickListener() {
			
			@Override
			public void onClick(DialogInterface dialog, int which) {
				saveChoosen();
				tempPref.edit().clear().commit();
				taskChooser.finish();
			}
         });

         // Setting Negative "NO" Button
         alertDialog.setNegativeButton("NO", new DialogInterface.OnClickListener() {
             public void onClick(DialogInterface dialog, int which) {
            	 
             }
         });

         alertDialog.show();
	}
	void saveChoosen(){
		for(int i=0;i<listTask.size();i++){
			//boolean b=tempPref.getBoolean("isSelected"+String.valueOf(i), false);
			//listTask.get(i).isSelected=b;
			
			pref.edit().putBoolean(String.valueOf(listTask.get(i).id)+"isSelected", mCheckedState[i]).commit();
		}
		pref.edit().putInt("ILE_NIEZROBIONYCH_DZIS", Math.min(N,listTask.size())).commit();
		pref.edit().putBoolean("CZY_DZIS_WYBRANE", true).commit();
	}
		
}

