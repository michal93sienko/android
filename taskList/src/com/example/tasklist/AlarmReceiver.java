package com.example.tasklist;

import java.util.Calendar;

import android.app.AlertDialog;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.media.AudioManager;
import android.os.Vibrator;
import android.support.v4.app.NotificationCompat;
import android.util.Log;
import android.widget.Toast;

public class AlarmReceiver extends BroadcastReceiver {
	static Context c;

	// private static AudioManager manager;
	@Override
	public void onReceive(Context context, Intent intent) {
		c = context;
		SharedPreferences pref = context.getSharedPreferences("ZADANIA",
				context.MODE_PRIVATE);
		int identyfikator;
		if ((identyfikator=intent.getExtras().getInt("idTask", -1)) != -1){
			Task t = new Task().taskFromPref(identyfikator, pref);
			createNotification("DO ZROBIENIA TERAZ!",t.getTitle(),"DO ZROBIENIA TERAZ",false);
		}else if (intent.getBooleanExtra("isDaily", false)) {

			createNotification("Wybierz zadania", "Wci�nij aby wybra� zadania na dze� dzisiejszy","Wybierz zadania" ,true);
			//Toast.makeText(context, "CODZIENNY", Toast.LENGTH_SHORT).show();

			SharedPreferences.Editor e = pref.edit();
			for (int i = 0; i < pref.getInt("LICZBA_REKORDOW", 0); i++) {
				e.putBoolean(String.valueOf(i) + "isSelected", false);
			}
			e.putBoolean("CZY_DZIS_WYBRANE", false);
			e.commit();
			wibruj();
		} else {
			// manager.setRingerMode(AudioManager.RINGER_MODE_NORMAL);
			//Toast.makeText(context, "INTERWA�OWY", Toast.LENGTH_SHORT).show();

			if (pref.getInt("ILE_NIEZROBIONYCH_DZIS", 0) != 0) {
				String tekst = "Dzi� masz jeszcze do zrobienia: " + "\n";
				String id = "";
				int liczbaTaskow = pref.getInt("LICZBA_REKORDOW", 0);
				for (int i = 0; i < liczbaTaskow; i++) {
					id = String.valueOf(i);
					if (!(pref.getBoolean(id + "isDone", false))
							&& pref.getBoolean(id + "isSelected", false))
						tekst += pref.getString(id + "tytul", "ERROR") + " \n";
				}
				Toast.makeText(c, tekst, Toast.LENGTH_LONG).show();
				wibruj();
			}
		}
		// Intent i = new Intent();
		// i.setAction("aktualizuj");
		// i.putExtra("isFirst", intent.getBooleanExtra("isFirst", false));
		// context.sendBroadcast(i);
		Log.d("RECEIVER", "�yjem");
		
	}

	public static void wibruj() {
		Vibrator v = (Vibrator) c.getSystemService(c.VIBRATOR_SERVICE);
		long pattern[] = { 0, 100, 200, 400, 200, 600, 200, 1000, 300, 2000 };
		v.vibrate(pattern, -1);
	}

	private void createNotification(String tytul, String opis, String ticker, boolean isTaskChooser) {
		Intent intent=null;
		if(isTaskChooser)
			intent= new Intent(c, TaskChooser.class);
		else
			intent = new Intent(c, MenuActivity.class);
		PendingIntent pIntent = PendingIntent.getActivity(c, 5, intent, 0);
		Bitmap icon = BitmapFactory.decodeResource(c.getResources(),
				R.drawable.ic_launcher);
		Notification noti = new NotificationCompat.Builder(c)
				.setContentTitle(tytul)
				.setContentText(opis)
				.setTicker(ticker)
				.setSmallIcon(android.R.drawable.ic_dialog_info)
				.setLargeIcon(icon).setAutoCancel(true)
				.setContentIntent(pIntent).build();
		NotificationManager notificationManager = (NotificationManager) c
				.getSystemService(c.NOTIFICATION_SERVICE);
		notificationManager.notify(0, noti);
	}

}
