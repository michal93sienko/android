package com.example.tasklist;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

import android.content.Context;
import android.content.SharedPreferences;

public class Task {
	private String title;
	public int id;
	private String description;
	public boolean images[];
	public boolean isDate;
	public Date data;
	public boolean hasImages;
	public int index;
	boolean isDone;
	boolean isSelected=false;
	
	public Task() {
		isDone=false;
		isDate=false;
		hasImages=false;
		images = new boolean[8];
		for(int i=0;i<8;i++) images[i]=false;
	}
	
	public Task taskFromPref(int id,SharedPreferences pref){
		this.id=id;
		String name = String.valueOf(id);
		//title = pref.getString(name+"title", "ERROR");
		//Task result = new Task(id,pref.getString(name+"tytul", "ERROR"),pref.getString(name+"opis", "ERROR"));
		this.title=pref.getString(name+"tytul", "ERROR");
		this.description=pref.getString(name+"opis", "ERROR");
		this.isDate=pref.getBoolean(name+"isDate", false);
		if(isDate){
			Calendar c = Calendar.getInstance();
			c.set(pref.getInt(name+"rok", 1999), pref.getInt(name+"miesiac", 1), pref.getInt(name+"dzien", 1));
			c.set(Calendar.HOUR_OF_DAY, pref.getInt(name+"godzina", 0));
			c.set(Calendar.MINUTE, pref.getInt(name+"minuta", 0));
			c.set(Calendar.SECOND,0);
			this.setDate(c.getTime());
		}
		this.hasImages=pref.getBoolean(name+"hasImages", false);
		if(hasImages){
			boolean zdjecia[] = new boolean[8];
			for(int i=0;i<8;i++){
				zdjecia[i]=pref.getBoolean(name+String.valueOf(i), false);
			}
			
			this.setImages(zdjecia);
		}
		this.setIndex(pref.getInt(name+"index", 100));
		this.isSelected=pref.getBoolean(name+"isSelected", false);
		this.setDone(pref.getBoolean(name+"isDone", false));
		return this;
	}
	
	public boolean isDone() {
		return isDone;
	}

	public void setDone(boolean isDone) {
		this.isDone = isDone;
	}

	public Task(int iden,String t, String desc){
		id=iden;
		isDone=false;
		isDate=false;
		hasImages=false;
		title = t;
		description=desc;
		images = new boolean[8];
		for(int i=0;i<8;i++) images[i]=false;
	}
	
	public void setDate(Date dat){
		isDate=true;
		data=dat;
	}
	
	public void setImages(boolean[] im){
		hasImages=true;
		for(int i=0;i<8;i++)images[i]=im[i];
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public int getIndex() {
		return index;
	}

	public void setIndex(int index) {
		this.index = index;
	}
	public int randomImage(){
		if(!this.hasImages) return -1;
		int size=0;
		int available[] = new int[8];
		for(int i=0;i<8;i++){
			if(images[i]){available[size]=i; size++;}
		}
		int ran = (int)(Math.random()*size);
		
		return available[ran];
	}
	

}
