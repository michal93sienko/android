package com.example.tasklist;

import java.util.Locale;

import android.app.Fragment;
import android.app.FragmentManager;
import android.content.Context;
import android.content.SharedPreferences;
import android.support.v13.app.FragmentPagerAdapter;

//import com.example.tasklist.MenuActivity.PlaceholderFragment;

public class SectionsPagerAdapter extends FragmentPagerAdapter {
	Context con;
	public static SharedPreferences pref;
	public static SharedPreferences prefTemp;
    public SectionsPagerAdapter(FragmentManager fm,Context c,SharedPreferences prefy, SharedPreferences temp) {
    	super(fm);
    	pref=prefy;
    	prefTemp=temp;
    	con = c;
        
        
    }

    @Override
    public Fragment getItem(int position) {
        // getItem is called to instantiate the fragment for the given page.
        // Return a PlaceholderFragment (defined as a static inner class below).
        return PlaceholderFragment.newInstance(position + 1,pref,prefTemp);
    }

    @Override
    public int getCount() {
        return 4;
    }

    @Override
    public CharSequence getPageTitle(int position) {
        Locale l = Locale.getDefault();
        switch (position) {
            case 0:
                return con.getResources().getString(R.string.title_section1).toUpperCase(l);
            case 1:
                return con.getResources().getString(R.string.title_section2).toUpperCase(l);
            case 2:
                return con.getResources().getString(R.string.title_section3).toUpperCase(l);
            case 3:
                return con.getResources().getString(R.string.title_section4).toUpperCase(l);
        }
        return null;
    }
}
