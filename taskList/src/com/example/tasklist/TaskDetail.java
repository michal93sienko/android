package com.example.tasklist;

import java.text.DateFormat;
import java.util.ArrayList;
import android.view.View.OnClickListener;
import android.os.Bundle;
import android.app.Activity;
import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.GridView;
import android.widget.TextView;
import android.widget.Toast;

public class TaskDetail extends Activity {
	GridView gv;
	Context context;
	public int images[];
	String from;
	// ArrayList prgmName;
	// public static String []
	// prgmNameList;//={"Let Us C","c++","JAVA","Jsp","Microsoft .Net","Android","PHP","Jquery","JavaScript"};
	// public static int []
	// Images;//={R.drawable.images,R.drawable.images1,R.drawable.images2,R.drawable.images3,R.drawable.images4,R.drawable.images5,R.drawable.images6,R.drawable.images7,R.drawable.images8};
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		int id = getIntent().getIntExtra("id", 2);
		 from= getIntent().getStringExtra("FROM");
		 
		super.onCreate(savedInstanceState);
		// Toast.makeText(this, "przej��em: " +String.valueOf(id),
		// Toast.LENGTH_LONG).show();

		setContentView(R.layout.activity_task_detail);

		context = getApplicationContext();
		final Task current = new Task();
		//Toast.makeText(context, "wychodze "+from, Toast.LENGTH_LONG).show();
		current.taskFromPref(id, getSharedPreferences("ZADANIA", MODE_PRIVATE));
		Button wykonane = (Button) findViewById(R.id.button1);
		TextView tytul = (TextView) findViewById(R.id.tytul);
		TextView data = (TextView) findViewById(R.id.data);
		TextView opis = (TextView) findViewById(R.id.opis);
		tytul.setText(current.getTitle());
		opis.setText(current.getDescription());
		if (current.isDone)
			wykonane.setVisibility(Button.INVISIBLE);
		// PlaceholderFragment.showOKDialog(getApplication().getApplicationContext(),
		// String.valueOf(current.hasImages));
		if (current.isDate) {
			data.setText("Data: "
					+ DateFormat.getDateTimeInstance().format(current.data));
		}
		if (current.hasImages) {
			int ile = 0;
			for (int i = 0; i < 8; i++) {
				if (current.images[i])
					ile++;
			}
			images = new int[ile];
			int x = 0;
			for (int i = 0; i < 8; i++) {
				if (current.images[i]) {
					int obraz = getResources().getIdentifier(
							"icon" + String.valueOf(i + 1), "drawable",
							getPackageName());
					images[x] = obraz;
					x++;
				}
			}
			gv = (GridView) findViewById(R.id.grid);
			gv.setNumColumns(5);
			boolean[] nic = new boolean[5];
			gv.setAdapter(new CustomAdapter(this, images, false, nic));
		}

		wykonane.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				current.isDone = true;
				SharedPreferences pref =  context.getSharedPreferences("ZADANIA", MODE_PRIVATE);
				if(pref.getBoolean(String.valueOf(current.id) + "isSelected", false)){//jesli wybrany na dzis
					int doZrobienia = pref.getInt("ILE_NIEZROBIONYCH_DZIS", 0);
					if(doZrobienia<=0)Toast.makeText(context, "ERROR", Toast.LENGTH_LONG ).show();
					else{
						doZrobienia--;
						pref.edit().putInt("ILE_NIEZROBIONYCH_DZIS", doZrobienia).commit();
					
					}
				}
				
				pref.edit()
						.putBoolean(String.valueOf(current.id) + "isDone", true)
						.putInt(String.valueOf(current.id) + "index", -1)
						.commit();
				if(current.isDate){//kasowane alarmu
					AlarmManager alarmManager = (AlarmManager)getSystemService(ALARM_SERVICE);
					Intent myIntent = new Intent("com.taskToDone");
					myIntent.putExtra("idTask", current.id);
					PendingIntent pendingIntent = PendingIntent.getBroadcast(getApplicationContext(), 777+current.id,
						myIntent, PendingIntent.FLAG_CANCEL_CURRENT);
					alarmManager.cancel(pendingIntent);
				}
				if (from!=null) {
				//	Toast.makeText(context, "wychodze", Toast.LENGTH_LONG).show();
					Intent i = new Intent(context,TaskChooser.class);
					i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
					startActivity(i);
				} else {
					Intent i = context
							.getPackageManager()
							.getLaunchIntentForPackage(context.getPackageName());
					i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
					startActivity(i);
				}
			}
		});
		//

	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.image_chooser, menu);
		return true;
	}

}
