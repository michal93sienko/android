package com.example.tasklist;
import java.text.DateFormat;
import java.util.ArrayList;
import java.util.List;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageManager;
import android.content.pm.PackageManager.NameNotFoundException;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.LayoutInflater.Filter;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.webkit.WebView.FindListener;
import android.widget.ArrayAdapter;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.RatingBar;
import android.widget.RatingBar.OnRatingBarChangeListener;
import android.widget.TextView;
import android.widget.Toast;

public class MyAdapter extends BaseAdapter {
	public List<Task> listTask;
	private final Context context;
	private final List<Task> all;
	public SharedPreferences pref;
	//private boolean isAdapter =true;
	
	public MyAdapter(List<Task> a, Context context,SharedPreferences pref) {
		listTask = a;
		this.pref=pref;
		this.context = context;
		all = new ArrayList<Task>(a);
	}

	@Override
	public int getCount() {
		// TODO Auto-generated method stub
		return listTask.size();
	}

	@Override
	public Task getItem(int arg0) {
		// TODO Auto-generated method stub
		return listTask.get(arg0);
	}

	@Override
	public long getItemId(int arg0) {
		// TODO Auto-generated method stub
		return arg0;
	}

	/*public android.widget.Filter getFilter() {

		android.widget.Filter filter = new android.widget.Filter() {

			// private List<String> arrayListNames;

			@Override
			protected void publishResults(CharSequence constraint,
					FilterResults results) {
				
				if (results.count == 0) {
					notifyDataSetInvalidated();
					MainActivity.listview.setAdapter(null);
					isAdapter=false;
					Toast.makeText(context, "Brak wynik�w", Toast.LENGTH_SHORT).show();
				} else {
					if(!isAdapter){
						MainActivity.listview.setAdapter(MyAdapter.this);
						isAdapter=true;
					}
					listaAplikacji = (List<ApplicationInfo>) results.values;
					notifyDataSetChanged();
				}
			}

			@Override
			protected FilterResults performFiltering(CharSequence constraint) {
				FilterResults results = new FilterResults();
				
				if (constraint.length() == 0) {
					results.values = all;
					results.count = all.size();
				} else {
					ArrayList<ApplicationInfo> FilteredList = new ArrayList<ApplicationInfo>();
					PackageManager pm = context.getPackageManager();
					//Toast.makeText(context, constraint, Toast.LENGTH_SHORT).show();
					for (ApplicationInfo info : all) {
						if (pm.getApplicationLabel(info).toString().toUpperCase()
								.startsWith(constraint.toString().toUpperCase())) {
							FilteredList.add(info);
						}
						results.values = FilteredList;
						results.count = FilteredList.size();
					}
				}
				return results;
			}
		};
		return filter;
	}
*/
	

	public int getPosition(Task arg) {
		return listTask.indexOf(arg);
	}

	@Override
	public View getView(final int position, View arg1, final ViewGroup parent) { // int
																		// position,
																		// View
																		// convertView,
																		// ViewGroup
																		// parent
		
		final Task current = listTask.get(position);
		//if(!current.isDone) return null;//?????????
		LayoutInflater inflater = (LayoutInflater) context
				.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		
		if (arg1 == null) {
			arg1 = inflater.inflate(R.layout.tastlist_to_done, parent, false);
		}
		View rowView = inflater.inflate(R.layout.task_row, parent, false);
		rowView.setClickable(true);
		
		ImageView up = (ImageView) rowView.findViewById(R.id.up);
		ImageView down = (ImageView) rowView.findViewById(R.id.down);
		TextView tytul = (TextView) rowView.findViewById(R.id.title);
		TextView opis = (TextView) rowView.findViewById(R.id.desc);
		ImageView imageView = (ImageView) rowView.findViewById(R.id.zdjecie);
//		final String nazwaAplikacji = context.getPackageManager()
//				.getApplicationLabel(listaAplikacji.get(position)).toString();
//		final Intent openApp = context.getPackageManager()
//				.getLaunchIntentForPackage(
//						listaAplikacji.get(position).packageName);
		tytul.setId(current.getIndex()); // /uwa�aj na to!!!
		String tekst = current.getTitle();
		if(current.isDate)
			tekst += "      "+ DateFormat.getDateTimeInstance().format(current.data);
		tytul.setText(tekst);
		opis.setText(current.getDescription());
		int numer= current.randomImage();
		if (numer  != -1) {
			String imageName = "icon"+String.valueOf(numer+1);// + String.valueOf(numer + 1);
			final int kolor = context.getResources().getIdentifier(imageName,
					"drawable", context.getPackageName());// kolor obrazek
			//Toast.makeText(context, String.valueOf(numer+1), Toast.LENGTH_LONG).show();
			imageView.setImageResource(kolor);
		}
		
		
				
				OnClickListener ocl =new OnClickListener() {//szczeg�y zadania
			
			@Override
			public void onClick(View v) {
				Intent i = new Intent(context,TaskDetail.class);
				i.putExtra("id", current.id);
				//Toast.makeText(context, "wys�a�e: "+String.valueOf(current.id), Toast.LENGTH_LONG).show();
				context.startActivity(i);
				
			}
		};
				tytul.setOnClickListener(ocl);
				imageView.setOnClickListener(ocl);
				if(current.isSelected)
				rowView.setBackgroundColor(Color.rgb(5, 200, 140));
		up.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				SharedPreferences.Editor e = pref.edit();
				int tmp = current.index;
				///Toast.makeText(context, "listener", Toast.LENGTH_LONG).show();
				int max = pref.getInt("LICZBA_REKORDOW", 0);
				if(position!=0){
					
					current.index--;
					Task drugi = listTask.get(position-1);
					drugi.index++;
					e.putInt(String.valueOf(current.id)+"index", current.index);
					e.putInt(String.valueOf(drugi.id)+"index", drugi.index);
					PlaceholderFragment.reorder(context ,current,(ArrayList<Task>)listTask,(ListView)parent);//, listview, taski)
					e.commit();
					
				}
				
			}
		});
		down.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				SharedPreferences.Editor e = pref.edit();
				int tmp = current.index;
				//Toast.makeText(context, "listener", Toast.LENGTH_LONG).show();
				int max = pref.getInt("LICZBA_REKORDOW", 0);
				if (position < listTask.size()-1) {
					Task drugi;
					current.index++;
					
					drugi = listTask.get(position + 1);
					drugi.index--;
					e.putInt(String.valueOf(current.id)+"index", current.index);
					e.putInt(String.valueOf(drugi.id)+"index", drugi.index);
					e.commit();
					PlaceholderFragment.reorder(context,current,(ArrayList<Task>)listTask,(ListView)parent);// ,
																		// listview,
																		// taski)
				}

			}
		});
//		try {
//			imageView.setBackground(context.getPackageManager()
//					.getApplicationIcon(
//							listaAplikacji.get(position).packageName));
//		} catch (NameNotFoundException e) {
//			e.printStackTrace();
//		}
//		imageView.setOnClickListener(new OnClickListener() {
//
//			@Override
//			public void onClick(View v) {
//				context.startActivity(openApp);
//				;
//
//			}
//		});
		
		// change the icon for Windows and iPhone
		// String s = values[position];
		// if (s.startsWith("iPhone")) {
		// imageView.setImageResource(R.drawable.no);
		// } else {
		// imageView.setImageResource(R.drawable.ok);
		// }

		return rowView;
		// TextView chapterName = (TextView)arg1.findViewById(R.id.textView1);
		// TextView chapterDesc = (TextView)arg1.findViewById(R.id.textView2);

		// ApplicationInfo chapter = listaAplikacji.get(position);

		// chapterName.setText(chapter.chapterName);
		// chapterDesc.setText(chapter.chapterDescription);

	}

}

