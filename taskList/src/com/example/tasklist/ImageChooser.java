package com.example.tasklist;

import android.app.Activity;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.net.Uri;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.view.Gravity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnTouchListener;
import android.view.ViewGroup.LayoutParams;
import android.view.ViewGroup.MarginLayoutParams;
import android.widget.Button;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ScrollView;
import android.widget.Toast;

public class ImageChooser extends Activity {
	public boolean choosen [];
	 CustomAdapter ca;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		//setContentView(R.layout.activity_image_chooser);
		
		choosen = new boolean[10];
		for(int i=0;i<8;i++)choosen[i]=false;
		
		 DisplayMetrics metrics = new DisplayMetrics();
		 int width = metrics.widthPixels;
		 
		 
		// LinearLayout linearLayout =  (LinearLayout)findViewById(R.id.linear);
		 LinearLayout linearLayout = new LinearLayout(this);
			linearLayout.setOrientation(LinearLayout.VERTICAL);
			linearLayout.setLayoutParams(new LayoutParams(
					LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT));
		
		GridView gv = new GridView(this);
		gv.setLayoutParams(new LayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT));
		//gv.setlay
		//gv.setColumnWidth(GridView.STRETCH_COLUMN_WIDTH);
		//gv.setStretchMode(140);
		gv.setNumColumns(GridView.AUTO_FIT);
		int []im = {1,2,3,4,5,6,7,8};
		if (savedInstanceState != null) {
			choosen=savedInstanceState.getBooleanArray("SELECTED");
			
			
		}
		ca =new CustomAdapter(this, im,true,choosen);
		gv.setAdapter(ca);
		
	//	for(int i=1;i<=8;i++)
		//	addImageView(linearLayout, i);
		
		Button b = new Button(this);
		/*LayoutParams params = new LayoutParams(
		        LayoutParams.WRAP_CONTENT,      
		        LayoutParams.WRAP_CONTENT
		);
		((MarginLayoutParams) params).setMargins(10,30,10,10);
		b.setLayoutParams(params);*/
		b.setText("ZAPISZ");
		
		b.setLayoutParams(new LayoutParams( 650, LayoutParams.WRAP_CONTENT));
		//LinearLayout.LayoutParams p =LayoutParams new Lin
		linearLayout.addView(gv);
		linearLayout.addView(b);
		LinearLayout.LayoutParams params = (android.widget.LinearLayout.LayoutParams) b.getLayoutParams();
		//params.width=0;
		//params.weight=(float)0.5;
		params.gravity = Gravity.CENTER;
		params.setMargins(10, 30, 10, 10);
		b.setTextSize(40);
		b.setLayoutParams(params);
		
		b.setOnClickListener(new OnClickListener() {//zaipsuje
			
			@Override
			public void onClick(View v) {
				SharedPreferences pref = getApplication().getSharedPreferences("TEMP", MODE_PRIVATE);
				SharedPreferences.Editor e = pref.edit();
				e.clear();
				choosen = ca.getItem(0);
				for(int j=0;j<8;j++)
					e.putBoolean(String.valueOf(j), choosen[j]);
				e.commit();
				finish();
			}
		});
		
		setContentView(linearLayout);
		/*ScrollView scrol = new ScrollView(this);
		scrol.setLayoutParams(new LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.WRAP_CONTENT));
		scrol.addView(linearLayout);
		setContentView(scrol);*/
		//
	}

	@Override
	protected void onSaveInstanceState(Bundle outState) {
		choosen = ca.getItem(0);
		outState.putBooleanArray("SELECTED", choosen);
		super.onSaveInstanceState(outState);
	}
	private void addImageView(LinearLayout ll, final int i) {
		final ImageView imageView = new ImageView(this);
		final String imageName = "icon" + i; // jpg?
		final int kolor = getResources().getIdentifier(imageName, "drawable",
				getPackageName());//kolor obrazek
		final int szary = getResources().getIdentifier(imageName+"grey", "drawable",
				getPackageName());//szary obrazek
		if (kolor != 0 && szary!=0)
			imageView.setImageResource(szary);
		else
			imageView.setImageResource(android.R.drawable.star_big_on);
		LinearLayout.LayoutParams lp = new LinearLayout.LayoutParams(200,200);
		lp.setMargins(10, 10, 10, 10);		
		
		imageView.setLayoutParams(lp);
		imageView.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				ImageView iv = (ImageView) v;
				if(choosen[i-1]){ //wybrany byl
					iv.setImageResource(szary);
					
				}else{
					iv.setImageResource(kolor);
				}
				choosen[i-1] = !choosen[i-1];
				
			}
		});
		
		ll.addView(imageView);
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.image_chooser, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		int id = item.getItemId();
		if (id == R.id.action_settings) {
			return true;
		}
		return super.onOptionsItemSelected(item);
	}
}
