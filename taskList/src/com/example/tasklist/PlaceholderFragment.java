package com.example.tasklist;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.Collections;

import android.app.AlarmManager;
import android.app.AlertDialog;
import android.app.Fragment;
import android.app.PendingIntent;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;
import android.widget.CompoundButton.OnCheckedChangeListener;

import com.example.tasklist.MenuActivity.Sortuj;
import com.google.android.gms.fitness.data.DataPoint;

public class PlaceholderFragment extends Fragment {
	public static ListView lv;
	public static MyAdapter ma;
	public static MyAdapter maZrobione;
	public  final static ArrayList<Task> listaTaskow=new ArrayList<Task>();
	public  final static ArrayList<Task> listaTaskowZrobionych=new ArrayList<Task>();
	public static SharedPreferences pref;
	public static SharedPreferences prefTemp;
	
	
	public static void reorder(Context c,final Task current,ArrayList<Task> taskList,final ListView lv){
		//Toast.makeText(c, "reorder", Toast.LENGTH_LONG).show();
		if(current==null)return;
		ma = new MyAdapter(taskList,c,  pref);
		Collections.sort(taskList, new Sortuj());
		lv.setAdapter(ma);
		lv.post(new Runnable() {
			@Override
			public void run() {
				lv.smoothScrollToPosition(ma.getPosition(current));
			}
		});
			
	}
    /**
     * The fragment argument representing the section number for this
     * fragment.
     */
    private static final String ARG_SECTION_NUMBER = "section_number";

    /**
     * Returns a new instance of this fragment for the given section
     * number.
     */
    public static PlaceholderFragment newInstance(int sectionNumber,SharedPreferences prefy,SharedPreferences temp) {
    	pref=prefy;
    	prefTemp=temp;
        PlaceholderFragment fragment = new PlaceholderFragment();
        Bundle args = new Bundle();
        args.putInt(ARG_SECTION_NUMBER, sectionNumber);
        fragment.setArguments(args);
        return fragment;
    }

    public PlaceholderFragment() {
    }

    @Override
    public void onResume() {
    	int sectionNumber = getArguments().getInt(ARG_SECTION_NUMBER, -1);
    	//Toast.makeText(getActivity(), "onResume " + sectionNumber , Toast.LENGTH_LONG).show();
    	if(sectionNumber == 3){
    		TextView tv = (TextView) getActivity().findViewById(R.id.listaNazw);
    		SharedPreferences pref = getActivity().getSharedPreferences("TEMP", getActivity().MODE_PRIVATE);
    		String result="Wybrane zdj�cia: ";
    		if(pref.contains("1")){
    			for(int i=0;i<8;i++)
    				if(pref.getBoolean(String.valueOf(i), false))
    				result+="icon"+String.valueOf(i+1)+".jpg, ";
    		}else
    			result+=" brak";
    		tv.setText(result);
    	}/*else if (sectionNumber==2){
    		listaTaskowZrobionych.clear();
			for (int i = 0; i < pref.getInt("LICZBA_REKORDOW", 0); i++) {
				Task temp = new Task().taskFromPref(i, pref);
				if (temp.isDone)
					listaTaskowZrobionych.add(temp);
			}
			if(listaTaskowZrobionych.size()>1){
				Collections.sort(listaTaskowZrobionych, new Sortuj());
				for(int i=0;i<listaTaskowZrobionych.size();i++){
					listaTaskowZrobionych.get(i).setIndex(i);
				}
			}
			
			ma = new MyAdapter(listaTaskowZrobionych, getActivity(), pref);
			lv.setAdapter(ma);
    	}else if(sectionNumber==1){
    		listaTaskow.clear();
			for (int i = 0; i < pref.getInt("LICZBA_REKORDOW", 0); i++) {
				Task temp = new Task().taskFromPref(i, pref);
				if (!temp.isDone){
					listaTaskow.add(temp);
				}
			}
			if(listaTaskow.size()>1){
				Collections.sort(listaTaskow, new Sortuj());
				for(int i=0;i<listaTaskow.size();i++){
					listaTaskow.get(i).setIndex(i);
				}
			}
			
			ma = new MyAdapter(listaTaskow, getActivity(), pref);
			lv.setAdapter(ma);
    	}*/
    	super.onResume();
    }
    void refreshList(boolean isDoneTasks,ArrayList<Task> lt, MyAdapter ma ){
    	
    }
    @Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		final View rootView;
		// SharedPreferences prefTemp =
		// getActivity().getSharedPreferences("TEMP", MODE_PRIVATE);
		prefTemp.edit().clear().commit();
		// final SharedPreferences pref =
		// this.getActivity().getSharedPreferences("ZADANIA", MODE_PRIVATE) ;
		int sectionNumber = getArguments().getInt(ARG_SECTION_NUMBER, -1);
		//Toast.makeText(getActivity(), "onCreateView " + sectionNumber , Toast.LENGTH_LONG).show();
		if (sectionNumber == 3) {

			rootView = inflater.inflate(R.layout.creator_fragment, container,
					false);

			loadCreator(rootView, pref, prefTemp);
		} else if (sectionNumber == 2) {
			rootView = inflater.inflate(R.layout.tastlist_done, container,
					false);
			lv = (ListView) rootView.findViewById(R.id.lista2);
			listaTaskowZrobionych.clear();
			for (int i = 0; i < pref.getInt("LICZBA_REKORDOW", 0); i++) {
				Task temp = new Task().taskFromPref(i, pref);
				if (temp.isDone)
					listaTaskowZrobionych.add(temp);
			}
			if(listaTaskowZrobionych.size()>1){
				Collections.sort(listaTaskowZrobionych, new Sortuj());
				for(int i=0;i<listaTaskowZrobionych.size();i++){
					listaTaskowZrobionych.get(i).setIndex(i);
				}
			}
			
			ma = new MyAdapter(listaTaskowZrobionych, getActivity(), pref);
			lv.setAdapter(ma);
		} else if (sectionNumber == 1) {
			rootView = inflater.inflate(R.layout.tastlist_to_done, container,
					false);
			lv = (ListView) rootView.findViewById(R.id.lista);
			// // = new List<>();
			listaTaskow.clear();
			for (int i = 0; i < pref.getInt("LICZBA_REKORDOW", 0); i++) {
				Task temp = new Task().taskFromPref(i, pref);
				if (!temp.isDone){
					listaTaskow.add(temp);
				}
			}
			if(listaTaskow.size()>1){
				Collections.sort(listaTaskow, new Sortuj());
				for(int i=0;i<listaTaskow.size();i++){
					listaTaskow.get(i).setIndex(i);
				}
			}
			
			ma = new MyAdapter(listaTaskow, getActivity(), pref);
			lv.setAdapter(ma);

		} else if (sectionNumber == 4) {
			rootView = inflater.inflate(R.layout.activity_alarm_setter, container,
					false);
			AlarmSetter a = new AlarmSetter(rootView, pref,getActivity());
		} else {
			rootView = inflater.inflate(R.layout.fragment_menu, container,
					false);
			TextView tv = (TextView) rootView.findViewById(R.id.section_label);
			tv.setText("" + sectionNumber);
		}
		return rootView;
	}

	protected void clearCreator(View rootView) {
		final EditText title = (EditText) rootView.findViewById(R.id.editText1);
		final EditText desc = (EditText) rootView.findViewById(R.id.editText2);
		final CheckBox cb = (CheckBox) rootView
				.findViewById(R.id.checkBox1);
		title.setText("");
		desc.setText("");
		cb.setChecked(false);
	}
	
	private boolean[] readPrefTemp(final SharedPreferences prefTemp){
		boolean result[] = new boolean[8];
		if(!prefTemp.contains(String.valueOf(1))){//jesli nie ma takich SharedPref w ogole
			for(int i=0;i<8;i++)
				result[i]=false;
			return result;
		}
		for(int i=0;i<8;i++)
			result[i]=prefTemp.getBoolean(String.valueOf(i), false);
		return result;
	}
	
	/*private Task readTaskFromPref(int id){
		//SharedPreferences pref = getActivity().getSharedPreferences("ZADANIA", MODE_PRIVATE);
		String name =String.valueOf(id);// pref.getString(String.valueOf(id), String.valueOf(-1)); //czyta name=title
		if(Integer.parseInt(name)<0) return new Task(-1,"ERROR","ERROR");
		
		Task result = new Task(id,pref.getString(name+"tytul", "ERROR"),pref.getString(name+"opis", "ERROR"));
		if(pref.getBoolean(name+"isDate", false)){
			Calendar c = Calendar.getInstance();
			c.set(pref.getInt(name+"rok", 1999), pref.getInt(name+"miesiac", 1), pref.getInt(name+"dzien", 1));
			c.set(Calendar.HOUR_OF_DAY, pref.getInt(name+"godzina", 0));
			c.set(Calendar.MINUTE, pref.getInt(name+"minuta", 0));
			result.setDate(c.getTime());
		}
		if(pref.getBoolean(name+"hasImages", false)){
			boolean zdjecia[] = new boolean[8];
			for(int i=0;i<8;i++){
				zdjecia[i]=pref.getBoolean(name+String.valueOf(i), false);
			}
			result.setIndex(pref.getInt(name+"index", 100));
			result.setDone(pref.getBoolean(name+"isDone", false));
			result.setImages(zdjecia);
		}
		return result;
	}
	*/
	protected void loadCreator(final View rootView,
			final SharedPreferences pref,final SharedPreferences prefTemp) {
		final EditText title = (EditText) rootView
				.findViewById(R.id.editText1);
		final EditText desc = (EditText) rootView
				.findViewById(R.id.editText2);
		final TimePicker tp = (TimePicker) rootView
				.findViewById(R.id.timePicker1);
		final Button b = (Button) rootView.findViewById(R.id.button1);
		final DatePicker datePicker = (DatePicker) rootView
				.findViewById(R.id.datePicker1);
		final CheckBox cb = (CheckBox) rootView
				.findViewById(R.id.checkBox1);
		final TextView tv = (TextView) rootView
				.findViewById(R.id.listaNazw);

		//datePicker.setEnabled(false);
		//tp.setEnabled(false);
		tp.setIs24HourView(true);
		datePicker.setMinDate(System.currentTimeMillis() - 1000);
		
		if(cb.isChecked()){
			datePicker.setVisibility(DatePicker.VISIBLE);
			tp.setVisibility(TimePicker.VISIBLE);
		}else{
			datePicker.setVisibility(DatePicker.INVISIBLE);
			tp.setVisibility(TimePicker.INVISIBLE);
		}

		cb.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				CheckBox c = (CheckBox)v;
				if(c.isChecked()){
					datePicker.setVisibility(DatePicker.VISIBLE);
					tp.setVisibility(TimePicker.VISIBLE);
				}else{
					datePicker.setVisibility(DatePicker.INVISIBLE);
					tp.setVisibility(TimePicker.INVISIBLE);
				}
			}
		});
		b.setOnClickListener(new OnClickListener() {// zapis

			@Override
			public void onClick(View v) {//zapis
				if(title.getText().length()==0)
				{showOKDialog(getActivity(),"TYTU� WYMAGANY");return;}
				int id = pref.getInt("LICZBA_REKORDOW", 0);
				SharedPreferences.Editor e = pref.edit();
				String name = String.valueOf(id);
				//e.putInt(name+ "id", id);
				e.putString(name+ "tytul", title.getText().toString());
				e.putInt(name+"index", id);
				
				//Toast.makeText(getActivity().getApplicationContext(), pref.getInt(name+"index", -1000), Toast.LENGTH_LONG).show();
				e.putString(name + "opis", desc.getText().toString());
				
						if (cb.isChecked()) {
							e.putBoolean(name + "isDate", true);
							e.putInt(name + "rok", datePicker.getYear());
							e.putInt(name + "miesiac", datePicker.getMonth());
							e.putInt(name + "dzien", datePicker.getDayOfMonth());
							e.putInt(name + "godzina", tp.getCurrentHour());
							e.putInt(name + "minuta", tp.getCurrentMinute());
						} else {
							e.putBoolean(name + "isDate", false);
						}
						
					
				
				
				boolean hasImages=false;
				String numberImages="";
				boolean images[] = readPrefTemp(prefTemp);
				for(int i=0;i<8;i++)if(images[i]==true){hasImages=true;break;} //sprawdza czy sa jakies zdjecia
				e.putBoolean(name+"hasImages", hasImages);
				if(hasImages){
					for(int i=0;i<8;i++){
						e.putBoolean(name+String.valueOf(i), images[i]);
						numberImages +=" "+ String.valueOf(images[i]);
					}
				}
					
				
				e.putBoolean(name+"isDone", false);
				e.putBoolean(name+"isSelected", false);
				e.putInt("LICZBA_REKORDOW", id + 1);
				e.commit();
				//showOKDialog(String.valueOf(pref.getInt(name+"index", -1000)));
				
				showOKDialog(getActivity(),"ZADANIE UTWORZONE ");
//				Toast.makeText(getActivity(), "Zadanie zapisane",
//						Toast.LENGTH_LONG).show();
				
				//LinearLayout ll = (LinearLayout) rootView
					//	.findViewById(R.id.setImage);
				/*LayoutParams params = new LayoutParams(
						LayoutParams.WRAP_CONTENT,
						LayoutParams.WRAP_CONTENT);
				b2.setLayoutParams(params);*/
				// ViewGroup viewGroup = (ViewGroup)
				// getView();//rootView.findViewById(R.id.scrollView1);
				//ll.addView(b2);
				// inaczej nie chce si� dodawa� w �rodku
				// http://www.mysamplecode.com/2011/10/android-dynamic-layout-using-xml-add.html
				if(cb.isChecked()){
				PendingIntent pendingIntent = null;
				Calendar calendar = Calendar.getInstance();
				calendar.set(datePicker.getYear(),datePicker.getMonth(),datePicker.getDayOfMonth());
				calendar.set(Calendar.HOUR_OF_DAY, tp.getCurrentHour());
				calendar.set(Calendar.MINUTE,tp.getCurrentMinute());
				calendar.set(Calendar.SECOND,0);
				Intent myIntent = new Intent("com.taskToDone");
				myIntent.putExtra("idTask", id); 
				pendingIntent = PendingIntent.getBroadcast(getActivity(), 700+id,
					myIntent, PendingIntent.FLAG_CANCEL_CURRENT);
				AlarmManager alarmManager = (AlarmManager) getActivity().getSystemService(getActivity().ALARM_SERVICE);
				alarmManager.set(AlarmManager.RTC_WAKEUP,calendar.getTimeInMillis(), 
						pendingIntent); 
				}
				clearCreator(rootView);
				datePicker.setVisibility(DatePicker.INVISIBLE);
				tp.setVisibility(TimePicker.INVISIBLE);
			}
		});
		Button b2 =(Button) rootView.findViewById(R.id.button2); //dodaj zdj�cia
		b2.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				Intent i = new Intent(getActivity(),ImageChooser.class);
				startActivity(i);
				
			}
		});
		/*cb.setOnCheckedChangeListener(new OnCheckedChangeListener() {

			@Override
			public void onCheckedChanged(CompoundButton buttonView,
					boolean isChecked) {
				datePicker.setEnabled(true);
				if (isChecked) {
					datePicker.setEnabled(true);
					tp.setEnabled(true);
				} else {
					datePicker.setEnabled(false);
					tp.setEnabled(false);
				}

			}
		});*/
	}
	 public static void showOKDialog(Context c,String tekst){
	    	AlertDialog.Builder builder = new AlertDialog.Builder(c);
	    	builder.setMessage(tekst)
	    	       .setCancelable(false)
	    	       .setPositiveButton("OK", new DialogInterface.OnClickListener() {
	    	           public void onClick(DialogInterface dialog, int id) {
	    	                //do things
	    	           }
	    	       });
	    	AlertDialog alert = builder.create();
	    	alert.show();
	    }
}
