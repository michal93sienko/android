package com.example.tasklist;

import java.util.ArrayList;
import java.util.zip.Inflater;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ListView;
import android.widget.Toast;

public class TaskChooser extends Activity {
	public final static ArrayList<Task> listaTaskow = new ArrayList<Task>();
	AdapterToTaskChooser ma;
	public boolean isAfterRotate = false;
	boolean[] checked = null;
	int zaznaczone;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		SharedPreferences pref = getSharedPreferences("ZADANIA", MODE_PRIVATE);
		if (pref.getBoolean("CZY_DZIS_WYBRANE", false)) {
			finish();
			Toast.makeText(
					this,
					"Ju� dzi� wybiera�e� \nNast�pne wybieranie o: "
							+ pref.getInt("godzinaAlarmu", 0) + ":"
							+ pref.getInt("minutaAlarmu", 0), Toast.LENGTH_LONG)

			.show();
		}
		if (savedInstanceState != null) {
			// isAfterRotate=savedInstanceState.getBoolean("isAfterRotate",false);
			// checked = new boolean[savedInstanceState.getInt("rozmiar",0)];
			checked = savedInstanceState.getBooleanArray("SELECTED");
			zaznaczone = savedInstanceState.getInt("COUNT_SELECTED", 0);
		}
		setContentView(R.layout.activity_task_chooser);

	}

	@Override
	protected void onResume() {

		ListView lv = (ListView) findViewById(R.id.lista);
		final SharedPreferences pref = getSharedPreferences("ZADANIA",
				Activity.MODE_PRIVATE);

		listaTaskow.clear();
		for (int i = 0; i < pref.getInt("LICZBA_REKORDOW", 0); i++) {
			Task temp = new Task().taskFromPref(i, pref);
			if (!temp.isDone) {
				listaTaskow.add(temp);
				if (isAfterRotate)
					listaTaskow.get(i).isSelected = true; // wczytujemy do
															// Taskow ich
															// prawidlowe
															// oznaczenia
			}
		}
		if (checked == null) {
			checked = new boolean[listaTaskow.size()];
			for (int i = 0; i < listaTaskow.size(); i++)
				checked[i] = false;
		}
		String test = "";

		ma = new AdapterToTaskChooser(listaTaskow, this, pref, isAfterRotate,
				checked, zaznaczone);
		lv.setAdapter(ma);
		super.onResume();
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.task_chooser, menu);

		return true;
	}

	@Override
	protected void onSaveInstanceState(Bundle outState) {
		outState.putBooleanArray("SELECTED", ma.getSelected());
		outState.putInt("COUNT_SELECTED", ma.countSelected());
		super.onSaveInstanceState(outState);
	}

	/*
	 * @Override protected void onSaveInstanceState(Bundle outState) {
	 * super.onSaveInstanceState(outState); outState.putBoolean("isAfterRotate",
	 * true); outState.putInt("rozmiar", listaTaskow.size()); for(int
	 * i=0;i<listaTaskow.size();i++) outState.putBoolean(String.valueOf(i) +
	 * "isSelected", listaTaskow.get(i).isSelected);;
	 * 
	 * }
	 * 
	 * @Override protected void onRestoreInstanceState(Bundle
	 * savedInstanceState) { super.onRestoreInstanceState(savedInstanceState);
	 * if(savedInstanceState!=null){ Toast.makeText(this, "jestem w restoreIf",
	 * Toast.LENGTH_LONG).show(); isAfterRotate =
	 * savedInstanceState.getBoolean("isAfterRotate",false); }
	 * 
	 * }
	 */
	@Override
	protected void onDestroy() {
		// getSharedPreferences("TEMP",
		// Context.MODE_PRIVATE).edit().clear().commit();
		super.onDestroy();
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		int id = item.getItemId();
		if (id == R.id.action_settings) {
			return true;
		}
		return super.onOptionsItemSelected(item);
	}
}
