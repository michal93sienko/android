package com.example.tasklist;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.Map;

import android.os.Bundle;
import android.os.DropBoxManager.Entry;
import android.app.Activity;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.view.Menu;
import android.view.View;

import android.widget.Button;
import android.view.View.OnClickListener;
import android.widget.Toast;

//import android.view.View;

public class FileChooser extends Activity {
	String m_chosen="";
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_file_chooser);
		//m_chosen="";
		//Button1
		Button dirChooserButton1 = (Button) findViewById(R.id.button1);
		dirChooserButton1.setOnClickListener(new OnClickListener() 
		{
			//String m_chosen;
			@Override
			public void onClick(View v) {
				/////////////////////////////////////////////////////////////////////////////////////////////////
				//Create FileOpenDialog and register a callback
				/////////////////////////////////////////////////////////////////////////////////////////////////
				SimpleFileDialog FileOpenDialog =  new SimpleFileDialog(FileChooser.this, "FileOpen",
						new SimpleFileDialog.SimpleFileDialogListener()
				{
					@Override
					public void onChosenDir(String chosenDir) 
					{
						// The code in this function will be executed when the dialog OK button is pushed 
						m_chosen = chosenDir;
						Toast.makeText(FileChooser.this, "Chosen FileOpenDialog File: " + 
								m_chosen, Toast.LENGTH_LONG).show();
					}
				});

				//You can change the default filename using the public variable "Default_File_Name"
				FileOpenDialog.Default_File_Name = "";
				FileOpenDialog.chooseFile_or_Dir();

				/////////////////////////////////////////////////////////////////////////////////////////////////

			}
		});

		//Button2
		Button dirChooserButton2 = (Button) findViewById(R.id.button2);
		dirChooserButton2.setOnClickListener(new OnClickListener() 
		{
			//String m_chosen;
			@Override
			public void onClick(View v) {
				/////////////////////////////////////////////////////////////////////////////////////////////////
				//Create FileSaveDialog and register a callback
				/////////////////////////////////////////////////////////////////////////////////////////////////
				SimpleFileDialog FileSaveDialog =  new SimpleFileDialog(FileChooser.this, "FileSave",
						new SimpleFileDialog.SimpleFileDialogListener()
				{
					@Override
					public void onChosenDir(String chosenDir) 
					{
						// The code in this function will be executed when the dialog OK button is pushed
						m_chosen = chosenDir;
						Toast.makeText(FileChooser.this, "Chosen FileOpenDialog File: " + 
								m_chosen, Toast.LENGTH_LONG).show();
					}
				});

				//You can change the default filename using the public variable "Default_File_Name"
				FileSaveDialog.Default_File_Name = "my_default.txt";
				FileSaveDialog.chooseFile_or_Dir();

				/////////////////////////////////////////////////////////////////////////////////////////////////

			}
		});

		//Button3
		Button dirChooserButton3 = (Button) findViewById(R.id.button3);
		dirChooserButton3.setOnClickListener(new OnClickListener() 
		{
			//String m_chosen;
			@Override
			public void onClick(View v) {

				/////////////////////////////////////////////////////////////////////////////////////////////////
				//Create FileOpenDialog and register a callback
				/////////////////////////////////////////////////////////////////////////////////////////////////
				SimpleFileDialog FolderChooseDialog =  new SimpleFileDialog(FileChooser.this, "FolderChoose",
						new SimpleFileDialog.SimpleFileDialogListener()
				{
					@Override
					public void onChosenDir(String chosenDir) 
					{
						// The code in this function will be executed when the dialog OK button is pushed
						m_chosen = chosenDir;
						Toast.makeText(FileChooser.this, "Chosen FileOpenDialog File: " + 
								m_chosen, Toast.LENGTH_LONG).show();
					}
				});

				FolderChooseDialog.chooseFile_or_Dir();

				/////////////////////////////////////////////////////////////////////////////////////////////////

			}
		});
		Button toast = (Button) findViewById(R.id.button4);
		toast.setOnClickListener(new OnClickListener() 
		{
			//String m_chosen;
			@Override
			public void onClick(View v) {
				Toast.makeText(getApplicationContext(), m_chosen, Toast.LENGTH_LONG).show();
			}

		});

	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.menu, menu);
		return true;
	}

	private boolean saveSharedPreferencesToFile(File dst) {
	    boolean res = false;
	    ObjectOutputStream output = null;
	    try {
	        output = new ObjectOutputStream(new FileOutputStream(dst));
	        SharedPreferences pref = 
	                            getSharedPreferences("ZADANIA", MODE_PRIVATE);
	        output.writeObject(pref.getAll());

	        res = true;
	    } catch (FileNotFoundException e) {
	        e.printStackTrace();
	    } catch (IOException e) {
	        e.printStackTrace();
	    }finally {
	        try {
	            if (output != null) {
	                output.flush();
	                output.close();
	            }
	        } catch (IOException ex) {
	            ex.printStackTrace();
	        }
	    }
	    return res;
	}

	@SuppressWarnings({ "unchecked" })
	private boolean loadSharedPreferencesFromFile(File src) {
	    boolean res = false;
	    ObjectInputStream input = null;
	    try {
	        input = new ObjectInputStream(new FileInputStream(src));
	            Editor prefEdit = getSharedPreferences("ZADANIA", MODE_PRIVATE).edit();
	            prefEdit.clear();
	            Map<String, ?> entries = (Map<String, ?>) input.readObject();
	            for (Map.Entry<String, ?> entry : entries.entrySet()) {
	                Object v = entry.getValue();
	                String key = entry.getKey();

	                if (v instanceof Boolean)
	                    prefEdit.putBoolean(key, ((Boolean) v).booleanValue());
	                else if (v instanceof Float)
	                    prefEdit.putFloat(key, ((Float) v).floatValue());
	                else if (v instanceof Integer)
	                    prefEdit.putInt(key, ((Integer) v).intValue());
	                else if (v instanceof Long)
	                    prefEdit.putLong(key, ((Long) v).longValue());
	                else if (v instanceof String)
	                    prefEdit.putString(key, ((String) v));
	            }
	            prefEdit.commit();
	        res = true;         
	    } catch (FileNotFoundException e) {
	        e.printStackTrace();
	    } catch (IOException e) {
	        e.printStackTrace();
	    } catch (ClassNotFoundException e) {
	        e.printStackTrace();
	    }finally {
	        try {
	            if (input != null) {
	                input.close();
	            }
	        } catch (IOException ex) {
	            ex.printStackTrace();
	        }
	    }
	    return res;
	}
}