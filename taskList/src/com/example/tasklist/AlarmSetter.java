package com.example.tasklist;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.Calendar;
import java.util.Date;
import java.util.Map;


import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TimePicker;
import android.widget.Toast;

public class AlarmSetter {
	public static EditText liczbaZad;
	public static EditText interwal;
	public static TimePicker tp;
	public static Button zapisz;
	public static Button clear;
	public static Button choose;
	public SharedPreferences pref;
	public Context c;
	int h;
	int m;
	private PendingIntent pendingIntent;
	String m_chosen="";
	AlarmSetter(View rootView,final SharedPreferences pref,final Context c) {
		this.c=c;
		this.pref=pref;
		
		liczbaZad = (EditText) rootView.findViewById(R.id.editText1);
		interwal = (EditText) rootView.findViewById(R.id.editText2);
		tp = (TimePicker) rootView.findViewById(R.id.timePicker1);
		tp.setIs24HourView(true);
		zapisz = (Button) rootView.findViewById(R.id.button1);
		clear = (Button) rootView.findViewById(R.id.button2);
		choose = (Button) rootView.findViewById(R.id.button3);
		
		

		if(pref.contains("Nzad")){
			liczbaZad.setText(String.valueOf(pref.getInt("Nzad", -1)));
			interwal.setText(String.valueOf(pref.getInt("inter", -2)));
			tp.setCurrentHour(pref.getInt("godzinaAlarmu", 0));
			tp.setCurrentMinute(pref.getInt("minutaAlarmu", 0));
		}
		
		choose.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				if(pref.getBoolean("CZY_DZIS_WYBRANE", false)){
					Toast.makeText(c, "Ju� dzi� wybiera�e� \nNast�pne wybieranie o: "+pref.getInt("godzinaAlarmu", 0) +":" + pref.getInt("minutaAlarmu", 0)	, Toast.LENGTH_LONG).show();
				}else{
				Intent i = new Intent(c,TaskChooser.class);
				c.startActivity(i);
				}

			}
		});
		
		clear.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				liczbaZad.setText("");
				interwal.setText("");
				tp.setCurrentHour(0);
				tp.setCurrentMinute(0);

			}
		});
		
		zapisz.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				if (interwal.getText().length() == 0
						|| liczbaZad.getText().length() == 0) {
					Toast.makeText(c, "NIEWPROWADZONE DANE", Toast.LENGTH_SHORT)
							.show();
					return;
				}
				int inter = Integer.parseInt(interwal.getText().toString());
				if (inter >= 24 * 60 || inter <= 0) {
					Toast.makeText(c, "NIEPOPRAWNY INTERWA�",
							Toast.LENGTH_SHORT).show();
					return;
				}
				int N = Integer.parseInt(liczbaZad.getText().toString());
				if (N <= 0) {
					Toast.makeText(c, "NIEPOPRAWNA LICZBA ZADA�",
							Toast.LENGTH_SHORT).show();
					return;
				}
				cancelAllAlarm(pref);
				 h = tp.getCurrentHour();
				 m = tp.getCurrentMinute();
				SharedPreferences.Editor e = pref.edit();
				e.putInt("Nzad", N);
				e.putInt("inter", inter);
				e.putInt("godzinaAlarmu", h);
				e.putInt("minutaAlarmu", m);
				e.commit();
				Toast.makeText(c, "ZAPISANE", Toast.LENGTH_SHORT).show();

				setAlarm(h, m, 24*60*60*1000, 1,c);
				setAlarm(h, m, inter*60*1000, 0,c);
			}
		});
		
		//String m_chosen="";
		Button eksport = (Button) rootView.findViewById(R.id.export);
		eksport.setOnClickListener(new OnClickListener() 
		{
			//String m_chosen;
			@Override
			public void onClick(View v) {

				/////////////////////////////////////////////////////////////////////////////////////////////////
				//Create FileOpenDialog and register a callback
				/////////////////////////////////////////////////////////////////////////////////////////////////
				SimpleFileDialog FileSaveDialog =  new SimpleFileDialog(c, "FileSave",
						new SimpleFileDialog.SimpleFileDialogListener()
				{
					@Override
					public void onChosenDir(String chosenDir) 
					{
						// The code in this function will be executed when the dialog OK button is pushed
						m_chosen = chosenDir;
						Toast.makeText(c, "Chosen FileOpenDialog File: " + 
								m_chosen, Toast.LENGTH_LONG).show();
						File target;
						if(m_chosen.endsWith(".xml")){
						 target = new File(m_chosen);
						 saveSharedPreferencesToFile(target);
						 
						 }
						else {Log.d("SAVE", "noXML");
						Toast.makeText(c, "prosze o rozszerzenie xml", Toast.LENGTH_LONG).show();
						}
					}
				});

				//You can change the default filename using the public variable "Default_File_Name"
				
				FileSaveDialog.Default_File_Name = "my_default.xml";
				FileSaveDialog.chooseFile_or_Dir();
				
				/////////////////////////////////////////////////////////////////////////////////////////////////

			}
		});
		
		
		Button importuj = (Button) rootView.findViewById(R.id.importuj);
		importuj.setOnClickListener(new OnClickListener() 
		{
			//String m_chosen;
			@Override
			public void onClick(View v) {

				/////////////////////////////////////////////////////////////////////////////////////////////////
				//Create FileOpenDialog and register a callback
				/////////////////////////////////////////////////////////////////////////////////////////////////
				SimpleFileDialog FileOpenDialog =  new SimpleFileDialog(c, "FileOpen",
						new SimpleFileDialog.SimpleFileDialogListener()
				{
					@Override
					public void onChosenDir(String chosenDir) 
					{
						// The code in this function will be executed when the dialog OK button is pushed
						m_chosen = chosenDir;
						Toast.makeText(c, "Chosen FileOpenDialog File: " + 
								m_chosen, Toast.LENGTH_LONG).show();
						File target;
						if(m_chosen.endsWith(".xml")){
						 target = new File(m_chosen);
						 loadSharedPreferencesFromFile(target);
						 Intent i = c.getApplicationContext().getPackageManager()
					             .getLaunchIntentForPackage( c.getApplicationContext().getPackageName() );
					i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
					c.startActivity(i);

						 }
						else {Log.d("LOAD", "noXML");
						Toast.makeText(c, "prosz� o rozszerzenie xml", Toast.LENGTH_LONG).show();
						}
					}
				});

				FileOpenDialog.Default_File_Name = "";
				FileOpenDialog.chooseFile_or_Dir();
				
				
				/////////////////////////////////////////////////////////////////////////////////////////////////

			}
		});
		
	}
	
	public static void setAlarm(int h, int min, int interwal, int idAlarm, Context c){
		
		//idAlarm:  1===>codzienny  0===>coInterwa�
		PendingIntent pendingIntent = null;
		Calendar calendar = Calendar.getInstance();
		calendar.set(Calendar.HOUR_OF_DAY, h);
		calendar.set(Calendar.MINUTE, min);
		calendar.set(Calendar.SECOND,0);
		if(calendar.compareTo(Calendar.getInstance())<=0){
			calendar.setTime(new Date( calendar.getTimeInMillis()+(24*60*60*1000))); //jesli ta godzina juz dzis byla to przestawiamy na dzien jutrzejszy
			Toast.makeText(c, "zaczynamy od jutra!", Toast.LENGTH_LONG).show();
		}
			if(idAlarm==0){//coInterwal
			calendar.setTime(new Date( calendar.getTimeInMillis()+interwal));//bedzie po interwale od ca�odziennego
			Intent myIntent = new Intent("com.remember");
			myIntent.putExtra("isDaily", false); 
			pendingIntent = PendingIntent.getBroadcast(c, idAlarm,
				myIntent, PendingIntent.FLAG_CANCEL_CURRENT);
		}else if(idAlarm==1){//co dzien
			Intent myIntent = new Intent("com.chooseTasks");
			myIntent.putExtra("isDaily", true); // wy��czaj�cy dzwonek
			pendingIntent = PendingIntent.getBroadcast(c, idAlarm,
				myIntent, PendingIntent.FLAG_CANCEL_CURRENT);
		}
				
		AlarmManager alarmManager = (AlarmManager) c.getSystemService(c.ALARM_SERVICE);
		alarmManager.setRepeating(AlarmManager.RTC_WAKEUP,calendar.getTimeInMillis(),interwal, 
				pendingIntent); 
	}
	
	public void cancelAllAlarm(SharedPreferences pref) {
		//pref.edit().clear().commit();
		SharedPreferences.Editor e = pref.edit();
		e.remove("Nzad");
		e.remove("inter");
		e.remove("minutaAlarmu");
		e.remove("godzinaAlarmu");
		e.commit();
		AlarmManager alarmManager = (AlarmManager) c.getSystemService(c.ALARM_SERVICE);
		Intent myIntent = new Intent(c, AlarmReceiver.class);
		pendingIntent = PendingIntent.getBroadcast(c, 1,
				myIntent, PendingIntent.FLAG_CANCEL_CURRENT);
		alarmManager.cancel(pendingIntent);
		pendingIntent = PendingIntent.getBroadcast(c, 0,
				myIntent, PendingIntent.FLAG_CANCEL_CURRENT);
		alarmManager.cancel(pendingIntent);
	}
	
	
	
	private boolean saveSharedPreferencesToFile(File dst) {
	    boolean res = false;
	    ObjectOutputStream output = null;
	    try {
	        output = new ObjectOutputStream(new FileOutputStream(dst));
	        SharedPreferences pref = 
	                            c.getSharedPreferences("ZADANIA", c.MODE_PRIVATE);
	        output.writeObject(pref.getAll());
	        //output.writeBytes("co z tego wyjdziesdfsdfsdfsdf?");
	        res = true;
	    } catch (FileNotFoundException e) {
	        e.printStackTrace();
	    } catch (IOException e) {
	        e.printStackTrace();
	    }finally {
	        try {
	            if (output != null) {
	                output.flush();
	                output.close();
	            }
	        } catch (IOException ex) {
	            ex.printStackTrace();
	        }
	    }
	    return res;
	}

	@SuppressWarnings({ "unchecked" })
	private boolean loadSharedPreferencesFromFile(File src) {
	    boolean res = false;
	    ObjectInputStream input = null;
	    try {
	        input = new ObjectInputStream(new FileInputStream(src));
	            Editor prefEdit = c.getSharedPreferences("ZADANIA", c.MODE_PRIVATE).edit();
	            prefEdit.clear();
	            Map<String, ?> entries = (Map<String, ?>) input.readObject();
	            for (Map.Entry<String, ?> entry : entries.entrySet()) {
	                Object v = entry.getValue();
	                String key = entry.getKey();

	                if (v instanceof Boolean)
	                    prefEdit.putBoolean(key, ((Boolean) v).booleanValue());
	                else if (v instanceof Float)
	                    prefEdit.putFloat(key, ((Float) v).floatValue());
	                else if (v instanceof Integer)
	                    prefEdit.putInt(key, ((Integer) v).intValue());
	                else if (v instanceof Long)
	                    prefEdit.putLong(key, ((Long) v).longValue());
	                else if (v instanceof String)
	                    prefEdit.putString(key, ((String) v));
	            }
	            prefEdit.commit();
	        res = true;         
	    } catch (FileNotFoundException e) {
	        e.printStackTrace();
	    } catch (IOException e) {
	        e.printStackTrace();
	    } catch (ClassNotFoundException e) {
	        e.printStackTrace();
	    }finally {
	        try {
	            if (input != null) {
	                input.close();
	            }
	        } catch (IOException ex) {
	            ex.printStackTrace();
	        }
	    }
	    return res;
	}
}
