package com.example.tasklist;

import android.content.Context;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.ViewGroup.LayoutParams;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

public class CustomAdapter extends BaseAdapter{

	String[] result;
	Context context;
	int[] images;
	boolean checked[];
	boolean isFromImageChooser;
	private static LayoutInflater inflater = null;

	public CustomAdapter(Context td, int[] currentimages, boolean isFromImageChooser,boolean [] ch) {
		// TODO Auto-generated constructor stub
		context = td;
		
		this.isFromImageChooser=isFromImageChooser;
		images = currentimages;
		checked = new boolean[images.length];
		checked=ch;
		//for(int i=0;i<checked.length;i++)checked[i]=false;

	}

	@Override
	public int getCount() {
		// TODO Auto-generated method stub
		
		return images.length;
	}

	@Override
	public boolean[] getItem(int position) {
		// TODO Auto-generated method stub
		return checked;
    }

    @Override
    public long getItemId(int position) {
        // TODO Auto-generated method stub
        return 0;
    }
/*
    public class Holder
    {
        TextView tv;
        ImageView img;
    }*/
    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
    	
    	
    	 inflater = (LayoutInflater) context
    			.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

    		View gridView= new View(context);
    		gridView = inflater.inflate(R.layout.image_list, null);
    		gridView.setLayoutParams(new LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.MATCH_PARENT));
    		ImageView iv =(ImageView) gridView.findViewById(R.id.imageView1);
    		//gridView.setBackgroundColor(Color.rgb(200, 100, 10));
    		if(!isFromImageChooser){
    		
    		iv.setImageResource(images[position]);
    		}else{
    			//definr szary=>kolor i vice versa
    			
			final int szary = context.getResources().getIdentifier(
					"icon" + String.valueOf(position + 1), "drawable",
					context.getPackageName());
			final int kolor = context.getResources().getIdentifier(
					"icon" + String.valueOf(position + 1) + "grey", "drawable",
					context.getPackageName());
			if (!checked[position]) {
				iv.setImageResource(kolor);
				iv.setTag("kolor");
			} else {
				iv.setImageResource(szary);
				iv.setTag("szary");
			}
			iv.setOnClickListener(new OnClickListener() {

					@Override
					public void onClick(View v) {
						ImageView image = (ImageView)v;
						if(image.getTag()=="kolor"){
							image.setImageResource(szary);
							image.setTag("szary");
						}
						else if(image.getTag()=="szary"){
							image.setImageResource(kolor);
							image.setTag("kolor");
						}
						checked[position]=!checked[position];
					}
				});
    		}
    		return gridView;
    }

}
