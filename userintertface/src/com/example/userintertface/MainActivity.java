package com.example.userintertface;

/*
 * User Story:
1. Jako klient mog� wybra� rodzaj pokoju w jakim chcia�bym mieszka�.
2. Jako klient mog� poda� dane osobowe i kontaktowe by umo�liwi� obs�udze realizacj� rezerwacji.
3. Jako klient mog� wybra� czy pok�j ma by� na niskich pi�trach czy na wy�szych by dopasowa� rezerwacj� do moich upodoba�.
4. Jako klient mog� wybra� czy pok�j ma by� dla pal�cych czy nie by dopasowa� rezerwacj� do moich preferencji zdrowotnych.
5. Jako klient mog� wybra� dodatki do pokoju: �niadanie, ��ko dla dziecka, p�ny check-interface
6. Jako klient mog� zdefiniowa� liczb� os�b (1 do 4), kt�re b�d� mieszka� w pokoju by m�c zrobi� rezerwacj� dla wi�cej ni� jednej osoby.
7. Jako klient mog� zobaczy� podsumowanie mojej rezerwacji przed jej ostatecznym potwierdzeniem by wychwyci� potencjalne pomy�ki.
8. Jako klient mog� zobaczy� cen� ca�kowit� z rozbiciem na poszczeg�lne elementy przed ostatecznym potwierdzeniem by upewni� si� co do koszt�w pobytu.

 */
import android.app.Activity;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;

public class MainActivity extends Activity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		FragmentManager fm = getFragmentManager();
		FragmentTransaction ft = fm.beginTransaction();
		ft.add(R.id.content, new Krok());
		ft.commit();
		
	
		
		//int wcisniety =1; //1 lub 2 lub 3

		final Button b1 = (Button) findViewById(R.id.step1);
		final Button b2 = (Button) findViewById(R.id.step2);
		final Button b3 = (Button) findViewById(R.id.sum);
		b1.setEnabled(false);

		b1.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                b2.setEnabled(true);
                b3.setEnabled(true);
                b1.setEnabled(false);
                FragmentManager fm = getFragmentManager();
                FragmentTransaction ft = fm.beginTransaction();
        		ft.replace(R.id.content, new Krok());
        		ft.commit();
            }
        });
		b2.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                b1.setEnabled(true);
                b3.setEnabled(true);
                b2.setEnabled(false);
                FragmentManager fm = getFragmentManager();
                FragmentTransaction ft = fm.beginTransaction();
        		ft.replace(R.id.content, new Krok2());
        		ft.commit();
            }
        });
		b3.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                b2.setEnabled(true);
                b1.setEnabled(true);
                b3.setEnabled(false);
                FragmentManager fm = getFragmentManager();
                FragmentTransaction ft = fm.beginTransaction();
        		ft.replace(R.id.content, new Sum());
        		ft.commit();
            }
        });
		
		
			
	

	}
	public void koniec(){
		onDestroy();
	}
	@Override
	public void onBackPressed() {
		return;
	}
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        if (id == R.id.action_settings) {
            return true;
        }
        return super.onOptionsItemSelected(item);
    }
}
