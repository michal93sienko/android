package com.example.userintertface;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

public class Sum extends android.app.Fragment {
  
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
        Bundle savedInstanceState) {
        // Inflate the layout for this fragment
    	 View view = inflater.inflate(R.layout.summary,
    		        container, false);
    	 return view;
    }
}
