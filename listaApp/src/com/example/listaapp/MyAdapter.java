package com.example.listaapp;

import java.util.ArrayList;
import java.util.List;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageManager;
import android.content.pm.PackageManager.NameNotFoundException;
import android.view.LayoutInflater;
import android.view.LayoutInflater.Filter;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.webkit.WebView.FindListener;
import android.widget.ArrayAdapter;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.RatingBar.OnRatingBarChangeListener;
import android.widget.TextView;
import android.widget.Toast;

public class MyAdapter extends BaseAdapter {
	public List<ApplicationInfo> listaAplikacji;
	private final Context context;
	private final List<ApplicationInfo> all;
	private boolean isAdapter =true;
	public MyAdapter(List<ApplicationInfo> a, Context context) {
		listaAplikacji = a;
		this.context = context;
		all = new ArrayList<ApplicationInfo>(a);
	}

	@Override
	public int getCount() {
		// TODO Auto-generated method stub
		return listaAplikacji.size();
	}

	@Override
	public ApplicationInfo getItem(int arg0) {
		// TODO Auto-generated method stub
		return listaAplikacji.get(arg0);
	}

	@Override
	public long getItemId(int arg0) {
		// TODO Auto-generated method stub
		return arg0;
	}

	public android.widget.Filter getFilter() {

		android.widget.Filter filter = new android.widget.Filter() {

			// private List<String> arrayListNames;

			@Override
			protected void publishResults(CharSequence constraint,
					FilterResults results) {
				
				if (results.count == 0) {
					notifyDataSetInvalidated();
					MainActivity.listview.setAdapter(null);
					isAdapter=false;
					Toast.makeText(context, "Brak wynik�w", Toast.LENGTH_SHORT).show();
				} else {
					if(!isAdapter){
						MainActivity.listview.setAdapter(MyAdapter.this);
						isAdapter=true;
					}
					listaAplikacji = (List<ApplicationInfo>) results.values;
					notifyDataSetChanged();
				}
			}

			@Override
			protected FilterResults performFiltering(CharSequence constraint) {
				FilterResults results = new FilterResults();
				
				if (constraint.length() == 0) {
					results.values = all;
					results.count = all.size();
				} else {
					ArrayList<ApplicationInfo> FilteredList = new ArrayList<ApplicationInfo>();
					PackageManager pm = context.getPackageManager();
					//Toast.makeText(context, constraint, Toast.LENGTH_SHORT).show();
					for (ApplicationInfo info : all) {
						if (pm.getApplicationLabel(info).toString().toUpperCase()
								.startsWith(constraint.toString().toUpperCase())) {
							FilteredList.add(info);
						}
						results.values = FilteredList;
						results.count = FilteredList.size();
					}
				}
				return results;
			}
		};
		return filter;
	}

	

	public int getPosition(ApplicationInfo arg) {
		return listaAplikacji.indexOf(arg);
	}

	@Override
	public View getView(int position, View arg1, ViewGroup parent) { // int
																		// position,
																		// View
																		// convertView,
																		// ViewGroup
																		// parent
		LayoutInflater inflater = (LayoutInflater) context
				.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		final SharedPreferences pref = context.getSharedPreferences("RANKING",
				Activity.MODE_PRIVATE);
		final int x = position;
		if (arg1 == null) {
			arg1 = inflater.inflate(R.layout.lista, parent, false);
		}
		View rowView = inflater.inflate(R.layout.row_layout, parent, false);
		rowView.setClickable(true);

		TextView textView = (TextView) rowView.findViewById(R.id.nazwa);
		ImageView imageView = (ImageView) rowView.findViewById(R.id.ikona);
		final String nazwaAplikacji = context.getPackageManager()
				.getApplicationLabel(listaAplikacji.get(position)).toString();
		final Intent openApp = context.getPackageManager()
				.getLaunchIntentForPackage(
						listaAplikacji.get(position).packageName);
		textView.setId(position); // /uwa�aj na to!!!
		textView.setText(nazwaAplikacji);
		try {
			imageView.setBackground(context.getPackageManager()
					.getApplicationIcon(
							listaAplikacji.get(position).packageName));
		} catch (NameNotFoundException e) {
			e.printStackTrace();
		}
		imageView.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				context.startActivity(openApp);
				;

			}
		});
		RatingBar rb = (RatingBar) rowView.findViewById(R.id.ranking);
		rb.setRating(pref.getFloat(nazwaAplikacji, 0));
		rb.setOnRatingBarChangeListener(new OnRatingBarChangeListener() {
			// http://stackoverflow.com/questions/21601465/a-list-of-favorites-in-android-re-change-the-rating-bar
			@Override
			public void onRatingChanged(RatingBar ratingBar, float rating,
					boolean fromUser) {
				// ratingBar.setRating(rating);
				if (fromUser) {
					SharedPreferences.Editor editor = pref.edit();
					editor.putFloat(nazwaAplikacji, rating);
					editor.commit();
					SharedPreferences pref = context.getSharedPreferences(
							"RANKING", Activity.MODE_PRIVATE);
					if (pref.getBoolean("RANK?", false)) { // je�li posortowane
															// po rankingu
						MainActivity.reorder(context, x);
					}

				}

			}
		});
		// change the icon for Windows and iPhone
		// String s = values[position];
		// if (s.startsWith("iPhone")) {
		// imageView.setImageResource(R.drawable.no);
		// } else {
		// imageView.setImageResource(R.drawable.ok);
		// }

		return rowView;
		// TextView chapterName = (TextView)arg1.findViewById(R.id.textView1);
		// TextView chapterDesc = (TextView)arg1.findViewById(R.id.textView2);

		// ApplicationInfo chapter = listaAplikacji.get(position);

		// chapterName.setText(chapter.chapterName);
		// chapterDesc.setText(chapter.chapterDescription);

	}

}
