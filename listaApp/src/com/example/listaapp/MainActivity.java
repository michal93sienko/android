package com.example.listaapp;

import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageManager;
import android.content.pm.PackageManager.NameNotFoundException;
import android.content.pm.ResolveInfo;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.text.Editable;
import android.text.InputType;
import android.text.TextWatcher;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.SearchView;
import android.widget.SearchView.OnQueryTextListener;
import android.widget.TextView;
import android.widget.Toast;
//http://www.vogella.com/tutorials/AndroidListView/article.html
//http://www.codelearn.org/android-tutorial/android-listview

public class MainActivity extends Activity {
	public  static List<ApplicationInfo> packages;
	public static ListView listview;
	public static MyAdapter ma;
	public static Context mContext;
	public static EditText et;
	public SearchView mSearchView;
	
	protected static class SortByRating implements Comparator<ApplicationInfo>{
		public SharedPreferences pref;
		public SortByRating(SharedPreferences p) {
			 pref=p;
		}
	    public int compare(ApplicationInfo o1, ApplicationInfo o2) {
	    	
	    	ApplicationInfo p1 = (ApplicationInfo) o1;
	    	ApplicationInfo p2 = (ApplicationInfo) o2; 
	    	
	    	String nazwaAplikacji1 = mContext.getPackageManager().getApplicationLabel( p1).toString();
	    	String nazwaAplikacji2 = mContext.getPackageManager().getApplicationLabel( p2).toString();
	    	float a = pref.getFloat(nazwaAplikacji1, 0);
	    	float b = pref.getFloat(nazwaAplikacji2, 0);
	        return  a > b ? -1 : a == b? 0 : 1;
	    }
	}
	
	protected static class SortByName implements Comparator{
		
	    public int compare(Object o1, Object o2) {
	    	ApplicationInfo p1 = (ApplicationInfo) o1;
	    	ApplicationInfo p2 = (ApplicationInfo) o2; 
	        return  mContext.getPackageManager().getApplicationLabel(p1).toString().compareToIgnoreCase(mContext.getPackageManager().getApplicationLabel(p2).toString()); //(p1.findViewById(R.id.nazwa).toString().compareToIgnoreCase(p2.findViewById(R.id.nazwa).toString())) ;
	    }
	}
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.lista);
		listview = (ListView) findViewById(R.id.listview);
		final SharedPreferences pref = getSharedPreferences("RANKING", Activity.MODE_PRIVATE);
		mContext=this;
		
		// LinearLayout ll = (LinearLayout) findViewById(R.id.ll);
		//
		// Intent mi = new Intent(Intent.ACTION_MAIN, null);
		// mi.addCategory(Intent.CATEGORY_LAUNCHER);
		// List<ResolveInfo> packages =
		// getPackageManager().queryIntentActivities(mi, 0);// ( mi, 0);
		packages = getPackageManager()
				.getInstalledApplications(PackageManager.GET_META_DATA);
		listview.setItemsCanFocus(false);
		pref.edit().putBoolean("RANK?", false).commit();
		//Collections.sort(packages, new SortByRating(pref));
		ma = new MyAdapter(packages, this);
		listview.setAdapter(ma);
		//Collections.sort( (List<View>) listview, new SortByName());
		
		
		//http://stackoverflow.com/questions/13527771/sort-adapter-in-android
		
		

		/*listview.setOnItemClickListener(new OnItemClickListener() {
			@Override
			public void onItemClick(AdapterView<?> parent, View view,
					int position, long id) {
				TextView tv = (TextView) view.findViewById(R.id.nazwa);
				String s = ((ApplicationInfo) parent.getAdapter().getItem(
						position)).packageName;
				Toast.makeText(getApplicationContext(), "byleco",
						Toast.LENGTH_LONG).show();

			}
		});*/
		// TextView tv = (TextView) findViewById(R.id.tt);
		//String array = "";
		// getView zwraca widok z adaptera i te sprawy convertView
		//Drawable icon;

		int i = 0;
		/*
		 * for (ApplicationInfo packageInfo : packages) { try { array += i + " "
		 * + getPackageManager().getApplicationLabel(packageInfo) .toString() +
		 * "\n"; // + //
		 * getPackageManager().getLaunchIntentForPackage(packageInfo
		 * .packageName) // + "\n"; ImageView im = new
		 * ImageView(MainActivity.this);
		 * im.setBackground(getPackageManager().getApplicationIcon(
		 * packageInfo.packageName)); LinearLayout.LayoutParams lp = new
		 * LinearLayout.LayoutParams(50,50); ll.addView(im,lp); i++; } catch
		 * (NameNotFoundException e) { // TODO Auto-generated catch block
		 * e.printStackTrace(); } }
		 */
		// tv.setText(array);
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.main, menu);
		mSearchView = (SearchView) menu.findItem(R.id.menu_search).getActionView();
		setupSearchView();
		//	mSearchView.set
//		et = (EditText) findViewById(R.id.menu_search);
//		et.addTextChangedListener(new TextWatcher() {
//			
//			@Override
//			public void onTextChanged(CharSequence s, int start, int before, int count) {
//				
//				
//			}
//			
//			@Override
//			public void beforeTextChanged(CharSequence s, int start, int count,
//					int after) {
//				// TODO Auto-generated method stub
//				
//			}
//			
//			@Override
//			public void afterTextChanged(Editable s) {
//				//ma.getFilter().filter(s);
//				Toast.makeText(mContext, "niewiemco", Toast.LENGTH_SHORT).show();
//			}
//		});
		
		return true;
	}
	 private void setupSearchView() {
         mSearchView.setIconifiedByDefault(true);
         mSearchView.setOnQueryTextListener(o);
         mSearchView.setQueryHint("Search Here");
     }
	 OnQueryTextListener o = new OnQueryTextListener() {
		
		@Override
		public boolean onQueryTextSubmit(String query) {
			//Toast.makeText(mContext, "submit", Toast.LENGTH_SHORT).show();
			return false;
		}
		
		@Override
		public boolean onQueryTextChange(String newText) {
			//Toast.makeText(mContext, "niewiemco", Toast.LENGTH_SHORT).show();
			ma.getFilter().filter(newText);
			return false;
		}
	};
	

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		int id = item.getItemId();
		if (id == R.id.action_settings) {
			return true;
		}
		SharedPreferences pref = getSharedPreferences("RANKING", Activity.MODE_PRIVATE);
		mSearchView.clearFocus();
		switch (id) {
		case R.id.rankSort:
			mSearchView.clearFocus();
			mSearchView.setQuery("", false);
			
			ma = new MyAdapter(packages, this);
			Collections.sort(packages, new SortByRating(pref));
			pref.edit().putBoolean("RANK?", true).commit();
			listview.setAdapter(ma);
			return true;
		
		
		case R.id.nameSort:
			mSearchView.clearFocus();
			mSearchView.setQuery("", false);
			pref.edit().putBoolean("RANK?", false).commit();
			ma = new MyAdapter(packages, this);
			Collections.sort(packages, new SortByName());
			listview.setAdapter(ma);
			return true;

		default:
			return super.onOptionsItemSelected(item);
		}
		
	}
	public static void reorder(Context c, int position){
		
		//Toast.makeText(c, "niewiemco", Toast.LENGTH_SHORT).show();
		SharedPreferences pref = c.getSharedPreferences("RANKING", Activity.MODE_PRIVATE);
		final ApplicationInfo current = ma.getItem(position);
		ma = new MyAdapter(packages, c);
		Collections.sort(packages, new SortByRating(pref));
		listview.setAdapter(ma);
		
		 listview.post(new Runnable() {
		        @Override
		        public void run() {
		        	listview.smoothScrollToPosition(ma.getPosition(current));
		        }
		    });
		
	}
	
}


/*
 * TEMATY

* Menu
* Zarz�dzanie danymi listy


------------------------------------------------------------------------------
ZADANIE

---------------------------------------
Zadanie 9.1 - Ranking aplikacji II

Do zadania 8.1 doda� nast�puj�c� funkcjonalno��:
- sortowanie aplikacji na li�cie - kolejno�� alfabetyczna po nazwie,
- sortowanie aplikacji na li�cie - na podstawie rankingu:
	-- zmiana oceny aplikacji powinna zmieni� jej pozycje przy tym sortowaniu;
	-- je�eli aplikacja zmienia miejsce na li�cie to lista powinna przesun�� si� do nowego miejsca aplikacji,
- filtr po nazwie aplikacji:
	-- u�ytkownik wpisuje pierwsze znaki nazwy aplikacji i wtedy lista zostaje zaw�ona do tych aplikacji, kt�rych nazwa zaczyna si� od wprowadzonych znak�w.


 */



/*
 * Programowanie Urz�dze� Mobilnych - Android Uniwersytet Jagiello�ski, 2015
 * 
 * Warsztaty: 2015-12-14
 * 
 * 
 * ------------------------------------------------------------------------------
 * TEMATY
 * 
 * ListView Adapter Intent
 * 
 * 
 * ------------------------------------------------------------------------------
 * ZADANIE
 * 
 * --------------------------------------- Zadanie 8.1 - Ranking aplikacji
 * 
 * Stworzy� aplikacj� listuj�c� wszystkie aplikacje zainstalowane w systemie.
 * Ka�da aplikacja zapisana jest w postaci trzech element�w: ikony, nazwy oraz
 * oceny. Ocena ma posta� gwiazdek (RatingBar) - skala 0 do 3. Element (jedna
 * aplikacja) tej listy jest pokazywany w dw�ch wierszach - pierwszy zawiera
 * ikon� aplikacji i jej nazw� a drugi ocen� (RatingBar). Oceny s�
 * zapami�tywane. Naci�ni�cie ikony powoduje uruchomienie danej aplikacji.
 * Klikniecie gwiazdek powoduje zmian� oceny i jej zapami�tanie.
 * 
 * UWAGA: Zadanie b�dzie rozbudowywane na kolejnych �wiczeniach.
 * 
 * 
 * MO�E SI� PRZYDA�:
 * 
 * 1. android.content.pm.PackageManager List<ResolveInfo>
 * queryIntentActivities(Intent intent, int flags) Intent
 * getLaunchIntentForPackage(String packageName)
 * 
 * -----------------------------------------------------------------------
 * Intent mi = new Intent(Intent.ACTION_MAIN, null);
 * mi.addCategory(Intent.CATEGORY_LAUNCHER); List<ApplicationInfo> packages =
 * context.getPackageManager().queryIntentActivities( mainIntent, 0);
 * -----------------------------------------------------------------------
 * 
 * 
 * -----------------------------------------------------------------------
 * List<ApplicationInfo> packages = pm
 * .getInstalledApplications(PackageManager.GET_META_DATA);
 * 
 * for (ApplicationInfo packageInfo : packages) {
 * 
 * Log.d(TAG, "Installed package :" + packageInfo.packageName); Log.d(TAG,
 * "Launch Activity :" + pm.getLaunchIntentForPackage(packageInfo.packageName));
 * 
 * } -----------------------------------------------------------------------
 * 
 * -----------------------------------------------------------------------
 * LayoutInflater layoutInflater =
 * (LayoutInflater)this.getSystemService(Context.LAYOUT_INFLATER_SERVICE); View
 * view = layoutInflater.inflate(R.layout.mylayout, item );
 * -----------------------------------------------------------------------
 */


