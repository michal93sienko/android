package com.example.battery;

import java.text.DateFormat;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.Date;

import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

public class MainActivity extends Activity {
	private static int arrayLen = 0;
	private static final String PREFERENCES_NAME = "DANE";
	private static final String PREFERENCES_TEXT_FIELD = "tab";
	private static final String PREFERENCES_SUM_FIELD = "suma";
	private SharedPreferences preferences;
	private static final long interval = 120000;
	private BatteryChangeReceiver bcr = null;
	private BatteryChangeReceiver bcr2 = null;

	private class BatteryChangeReceiver extends BroadcastReceiver {
		@Override
		public void onReceive(Context context, Intent intent) {
			// Toast.makeText(context, "wewn receiuver !",
			// Toast.LENGTH_SHORT).show();

			String action = intent.getAction();
			TextView tv = (TextView) findViewById(R.id.tv);
			String array = tv.getText().toString();
			array += preferences.getString("LAST", "nimamnic");
			tv.setText(array);
		}
	}

	@Override
	protected void onStart() {
		super.onStart();
		IntentFilter filter = new IntentFilter("aktualizuj");
		bcr = new BatteryChangeReceiver();
		registerReceiver(bcr, filter);

	}

	@Override
	protected void onStop() {
		super.onStop();
		if (bcr != null) {
			unregisterReceiver(bcr);
			bcr = null;
		}
	}

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		preferences = getSharedPreferences(PREFERENCES_NAME,
				Activity.MODE_PRIVATE);
		SharedPreferences.Editor preferencesEditor = preferences.edit();
		if (!preferences.contains(PREFERENCES_TEXT_FIELD)) //za pierwszym razem odpalamy
			preferencesEditor.putInt(PREFERENCES_TEXT_FIELD, 0); // liczba
																	// wierszy
		// preferencesEditor.putInt(PREFERENCES_TEXT_FIELD, 232);
		if (!preferences.contains(PREFERENCES_SUM_FIELD))
			preferencesEditor.putLong(PREFERENCES_SUM_FIELD, 0);
		preferencesEditor.commit();
		displayRecords(getApplicationContext());

	}

	public void res(View v) {
		preferences.edit().clear().commit();
		TextView tv = (TextView) findViewById(R.id.tv);
		tv.setText("");
	}

	public void displayRecords(Context mContext) { // wy�wietla rekordy na
													// TextView

		TextView tv = (TextView) findViewById(R.id.tv);
		arrayLen = preferences.getInt(PREFERENCES_TEXT_FIELD, -1);
		String array = "";
		long now = new Date().getTime();
		long suma = 0;
		long firstRozp;
		long sumaFul = preferences.getLong(PREFERENCES_SUM_FIELD, 0);
		// 24*60*60*1000; //jedna doba
		// Toast.makeText(getApplication(), "nie wywalam si�",
		// Toast.LENGTH_SHORT).show();
		for (int i = 0; i < arrayLen; i++) {
			long rozp = preferences.getLong(
					PREFERENCES_TEXT_FIELD + i + "rozp", -8);
			long zak = preferences.getLong(PREFERENCES_TEXT_FIELD + i + "zak",
					-7);

			String dataRozp = DateFormat.getDateTimeInstance().format(
					new Date(rozp));
			String dataZak = DateFormat.getDateTimeInstance().format(
					new Date(zak));
			long diff = preferences.getLong(
					PREFERENCES_TEXT_FIELD + i + "diff", 0);
			if (rozp > now - interval)
				suma += diff;
			else if (zak > now - interval && rozp < now - interval)
				suma += zak - (now - interval);
			// if(!preferences.contains(PREFERENCES_TEXT_FIELD +
			// String.valueOf(i)))
			// Toast.makeText(getApplication(), "Brakuje "+i,
			// Toast.LENGTH_SHORT).show();
			array += data(rozp) + "   " + data(zak) + "   " + diff / 1000
					+ "\n";
		}
		long lastRozp;

		if (preferences.contains(PREFERENCES_TEXT_FIELD
				+ String.valueOf(arrayLen))) { // rozpocz�te �adowanie
			lastRozp = preferences.getLong(
					PREFERENCES_TEXT_FIELD + String.valueOf(arrayLen), 0);
			long diff = now - lastRozp;
			if (lastRozp > now - interval)
				suma += diff;
			else
				suma = interval; // trwa przez ca�y interval
			sumaFul += diff;
			array += data(lastRozp) + "   ";
		}
		NumberFormat formatter = new DecimalFormat();
		formatter.setMaximumFractionDigits(2);
		formatter.setMinimumFractionDigits(2);
		firstRozp = preferences.getLong(PREFERENCES_TEXT_FIELD + "0rozp", -5);
		double d = 1. * suma / 1000;
		tv.setText(array);
		array = "";
		array += "Suma w interwale : " + String.format("%.4f", d)
				+ "    interwal = " + interval + "ms\n";
		array += "�rednia : "
				+ String.format("%.4f", 1. * sumaFul
						/ (1000 * (now - firstRozp) / interval)) + "/"
				+ interval / 1000 + "sekund" + "\n";
		tv = (TextView) findViewById(R.id.tv2);
		tv.setText(array);

	}

	private String data(long r) {
		return String.valueOf(android.text.format.DateFormat.format(
				"dd MMM HH:mm:ss", new Date(r)));
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		int id = item.getItemId();
		if (id == R.id.action_settings) {
			return true;
		}
		return super.onOptionsItemSelected(item);
	}
}
