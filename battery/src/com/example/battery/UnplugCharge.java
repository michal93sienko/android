package com.example.battery;

import java.text.DateFormat;
import java.util.Date;

import android.app.Activity;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.IntentSender.SendIntentException;
import android.support.v4.app.NotificationCompat;
import android.widget.Toast;

public class UnplugCharge extends BroadcastReceiver {
	private int arrayLen;
	private long suma;
	private static final String PREFERENCES_NAME = "DANE";
	private static final String PREFERENCES_TEXT_FIELD = "tab";
	private static final String PREFERENCES_SUM_FIELD = "suma";
	private SharedPreferences preferences;

	@Override
	public void onReceive(Context context, Intent intent) {
		//Toast.makeText(context, "jestem w onReceive manifestowym!", Toast.LENGTH_SHORT).show();
		String action = intent.getAction();
		preferences = context.getSharedPreferences(PREFERENCES_NAME,
				Activity.MODE_PRIVATE);

		arrayLen = preferences.getInt(PREFERENCES_TEXT_FIELD, -1);
		SharedPreferences.Editor editor = preferences.edit();

		// *********Gdy pod�aczamy**********//

		if (action.equals(Intent.ACTION_POWER_CONNECTED)) {
			/*Toast.makeText(
					context,
					"rozpocz�ci� �adowania arrayLenn = " + arrayLen
							+ "\n bede pisa� pod " + PREFERENCES_TEXT_FIELD
							+ String.valueOf(arrayLen), Toast.LENGTH_SHORT)
					.show();*/
			if (arrayLen == -1)
				Toast.makeText(context, "co�Nie�miga -1", Toast.LENGTH_LONG)
						.show();
			long r = new Date().getTime();
			editor.putLong(PREFERENCES_TEXT_FIELD + String.valueOf(arrayLen),
					r);
			editor.putString("LAST", android.text.format.DateFormat.format("dd MMM HH:mm:ss",new Date(r))+"   ");
					//String.valueOf(r));
			
			
			// *********Gdy od��czamy**********//

		} else if (action.equals(Intent.ACTION_POWER_DISCONNECTED)) {
			suma = preferences.getLong(PREFERENCES_SUM_FIELD, 0);
			/*Toast.makeText(context, "zako�czenie �adowania", Toast.LENGTH_SHORT)
					.show();*/
			long dataRozp = preferences.getLong(
					PREFERENCES_TEXT_FIELD + String.valueOf(arrayLen),
					new Date().getTime()); //jesli nei ma jeszcze �adnej daty to nie liczy pierwszego �adowania. bedzie=0
			
			long dataZak = new Date().getTime();
			long diff = (dataZak - dataRozp);//w milisekundach b�dzie liczy�
			suma += diff;
			editor.putLong(PREFERENCES_SUM_FIELD, suma);
/* Format tabeli:   tab0rozp  tab0zak tab0diff */
			editor.putLong(PREFERENCES_TEXT_FIELD + String.valueOf(arrayLen)+"rozp", dataRozp);
			editor.putLong(PREFERENCES_TEXT_FIELD + String.valueOf(arrayLen)+"zak", dataZak);
			editor.putLong(PREFERENCES_TEXT_FIELD + String.valueOf(arrayLen)+"diff", diff);
			editor.putString("LAST",android.text.format.DateFormat.format("dd MMM HH:mm:ss",new Date(dataZak))+"   " + String.valueOf(diff/1000)+"\n");
			
			
			// wrzuca do Shered rekord w formacie: dataRozp (ms) dataZak(ms)
			// r�znica
			arrayLen++;
			editor.putInt(PREFERENCES_TEXT_FIELD, arrayLen);
			
			//notyfikacja//
			NotificationCompat.Builder mBuilder;
			 
			mBuilder = new NotificationCompat.Builder(context)
			.setSmallIcon(android.R.drawable.ic_dialog_info)
			.setContentTitle("Zako�czono �adowanie")
			.setAutoCancel(true)
			.setContentText("�adowa�em: "+String.format("%.2f",1.*diff/1000)+"s");
			 
			// Sets an ID for the notification
			int mNotificationId = 001;
			 Intent mIntent = new Intent(context, MainActivity.class);
	            PendingIntent mPendingIntent = PendingIntent.getActivity(context, 0, mIntent, 0);
	             
	            // I go ��czymy
	            mBuilder.setContentIntent(mPendingIntent);
			// Gets an instance of the NotificationManager service
			NotificationManager mNotifyMgr =
			(NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
			 
			// Builds the notification and issues it.
			mNotifyMgr.cancel("wywalamy", mNotificationId);
			mNotifyMgr.notify(mNotificationId,mBuilder.build());
			
		}
		
		editor.commit();
		
                
		Intent i = new Intent();
		i.setAction("aktualizuj");
		i.putExtra("name", "brak");
		context.sendBroadcast(i);
		
		//Toast.makeText(context, "wys���em aktualizacj� !",Toast.LENGTH_SHORT).show();
		/*
		 * preferences = context.getSharedPreferences(PREFERENCES_NAME,
		 * Activity.MODE_PRIVATE); arrayLen =
		 * preferences.getInt(PREFERENCES_TEXT_FIELD, -1);
		 * SharedPreferences.Editor editor = preferences.edit();
		 * 
		 * // editor.putInt(PREFERENCES_TEXT_FIELD, 232); if (arrayLen == -1)
		 * Toast.makeText(context, "co�Nie�miga -1", Toast.LENGTH_LONG).show();
		 * long dataRozp = Long.parseLong(preferences.getString(
		 * PREFERENCES_TEXT_FIELD + String.valueOf(arrayLen), "-10")); long
		 * dataZak = new Date().getTime(); long diff = (dataZak - dataRozp) /
		 * 1000 % 60; // liczy r�znic� editor.putString(PREFERENCES_TEXT_FIELD +
		 * String.valueOf(arrayLen), String.valueOf(dataRozp) + "  " +
		 * String.valueOf(dataZak) + "  " + String.valueOf(diff)); //wrzuca do
		 * Shered rekord w formacie: dataRozp (ms) dataZak(ms) r�znica
		 * 
		 * Toast.makeText(context, "zako�czy�em dodawanie",
		 * Toast.LENGTH_SHORT).show(); arrayLen++;
		 * editor.putInt(PREFERENCES_TEXT_FIELD, arrayLen); editor.commit();
		 */
	}

}

/*
 * <application android:icon="@drawable/icon" android:label="@string/app_name">
 * <receiver android:name=".receiver.PlugInControlReceiver"> <intent-filter>
 * <action android:name="android.intent.action.ACTION_POWER_CONNECTED" />
 * <action android:name="android.intent.action.ACTION_POWER_DISCONNECTED" />
 * </intent-filter> </receiver> </application> In Code
 * 
 * public class PlugInControlReceiver extends BroadcastReceiver { public void
 * onReceive(Context context , Intent intent) { String action =
 * intent.getAction();
 * 
 * if(action.equals(Intent.ACTION_POWER_CONNECTED)) { // Do something when power
 * connected } else if(action.equals(Intent.ACTION_POWER_DISCONNECTED)) { // Do
 * something when power disconnected } } }
 */
