package com.example.battery;

import java.util.Date;

import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Toast;

public class PlugCharge extends BroadcastReceiver {
	private int arrayLen;
	private static final String PREFERENCES_NAME = "DANE";
	private static final String PREFERENCES_TEXT_FIELD = "tab";
	private SharedPreferences preferences;

	@Override
	public void onReceive(Context arg0, Intent arg1) {
		preferences = arg0.getSharedPreferences(PREFERENCES_NAME,
				Activity.MODE_PRIVATE);
		arrayLen = preferences.getInt(PREFERENCES_TEXT_FIELD, -1);
		SharedPreferences.Editor editor = preferences.edit();

		// editor.putInt(PREFERENCES_TEXT_FIELD, 232);
		if(arrayLen == -1) Toast.makeText(arg0, "co�Nie�miga -1", Toast.LENGTH_LONG).show();
		editor.putString(PREFERENCES_TEXT_FIELD + String.valueOf(arrayLen),
				String.valueOf(new Date().getTime())); 
		// wrzuca tylko dat� ropocz�cia jako String z milisekund
		
		editor.commit();

	}

}
