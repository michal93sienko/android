package com.example.childgame;

import android.app.Activity;
import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;
//ta java tutaj nie pasuje mi co�.
public class MainActivity extends Activity {
	int wynik = 0;
	private int correctPlace=-1;
	private String pytanie_1="Serce?";
	private String pytanie_2="Telefon?";
	private String pytanie_3="S�o�ce?";
	private String[] questions={pytanie_1,pytanie_2,pytanie_3};
	private int numberOfQuestion=0;
	private  ImageButton ib1 ;
	private  ImageButton ib2 ;
	private  ImageButton ib3 ;
	private  ImageButton ib4 ;
	private static TextView tv;
	String sending;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		ib1 = (ImageButton) findViewById(R.id.imageButton1);
		ib2 = (ImageButton) findViewById(R.id.imageButton2);
		ib3 = (ImageButton) findViewById(R.id.imageButton3);
		ib4 = (ImageButton) findViewById(R.id.imageButton4);
		tv = (TextView) findViewById(R.id.textView1);
		graj();
		
	}
	private void graj(){
		if(correctPlace==-1){ //znaczy �e rozpoczelismy gr� lub next pytanie
			selectGoodPlace();
			setImage();			
		}
		else{//obr�t albo SharedP
			setImage();
		}
	}
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.main, menu);

		return true;
	}
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		int id = item.getItemId();
		if (id == R.id.action_settings) {
			return true;
		}
		return super.onOptionsItemSelected(item);
	}
	private void setImage(){
		tv.setText(questions[numberOfQuestion]);
		Drawable succes;
		Drawable b1;
		Drawable b2;
		Drawable b3;
		if(numberOfQuestion==0){
			String goodUri = "@drawable/serce";
			int imageResource = getResources().getIdentifier(goodUri, null, getPackageName());
			succes = getResources().getDrawable(imageResource);
			String bad1 = "@drawable/slonce";
			imageResource = getResources().getIdentifier(bad1, null, getPackageName());
			 b1 = getResources().getDrawable(imageResource);
			String bad2 = "@drawable/joy";
			imageResource = getResources().getIdentifier(bad2, null, getPackageName());
			 b2 = getResources().getDrawable(imageResource);
			String bad3 = "@drawable/komorka";
			imageResource = getResources().getIdentifier(bad3, null, getPackageName());
			 b3 = getResources().getDrawable(imageResource);
		} else if(numberOfQuestion==1){
			String goodUri = "@drawable/komorka";
			int imageResource = getResources().getIdentifier(goodUri, null, getPackageName());
			 succes = getResources().getDrawable(imageResource);
			String bad1 = "@drawable/slonce";
			imageResource = getResources().getIdentifier(bad1, null, getPackageName());
			 b1 = getResources().getDrawable(imageResource);
			String bad2 = "@drawable/joy";
			imageResource = getResources().getIdentifier(bad2, null, getPackageName());
			 b2 = getResources().getDrawable(imageResource);
			String bad3 = "@drawable/serce";
			imageResource = getResources().getIdentifier(bad3, null, getPackageName());
			 b3 = getResources().getDrawable(imageResource);
		} else {
			String goodUri = "@drawable/slonce";
			int imageResource = getResources().getIdentifier(goodUri, null, getPackageName());
			 succes = getResources().getDrawable(imageResource);
			String bad1 = "@drawable/serce";
			imageResource = getResources().getIdentifier(bad1, null, getPackageName());
			 b1 = getResources().getDrawable(imageResource);
			String bad2 = "@drawable/joy";
			imageResource = getResources().getIdentifier(bad2, null, getPackageName());
			 b2 = getResources().getDrawable(imageResource);
			String bad3 = "@drawable/komorka";
			imageResource = getResources().getIdentifier(bad3, null, getPackageName());
			 b3 = getResources().getDrawable(imageResource);
		}
		if(correctPlace==1){
			ib1.setImageDrawable(succes);
			ib2.setImageDrawable(b1);
			ib3.setImageDrawable(b2);
			ib4.setImageDrawable(b3);
		} else if(correctPlace==2){
			ib2.setImageDrawable(succes);
			ib1.setImageDrawable(b1);
			ib3.setImageDrawable(b2);
			ib4.setImageDrawable(b3);
		} else if(correctPlace==3){
			ib3.setImageDrawable(succes);
			ib2.setImageDrawable(b1);
			ib1.setImageDrawable(b2);
			ib4.setImageDrawable(b3);
		} else  {
			ib4.setImageDrawable(succes);
			ib2.setImageDrawable(b1);
			ib3.setImageDrawable(b2);
			ib1.setImageDrawable(b3);
		} 
			
	}
	public void next1(View w) { // dla button1
		if(correctPlace==1)wynik++;
		next();
	}

	public void next2(View w) { // dla button1
		if(correctPlace==2)wynik++;
		next();
	}

	public void next3(View w) { // dla button1
		if(correctPlace==3)wynik++;
		next();
	}

	public void next4(View w) { // dla button1
		if(correctPlace==4)wynik++;
		next();
	}

	private void next() { // og�lny!
		numberOfQuestion++;
		correctPlace=-1;
		Toast.makeText(getApplicationContext(), String.valueOf(wynik), Toast.LENGTH_LONG).show();

		if(numberOfQuestion<3) graj();
		else{
			sending="";
			Intent i = new Intent(this,ResultActivity.class);
			sending=""+wynik;
			i.putExtra("name", sending);
			startActivity(i);
		}
	}

	private void selectGoodPlace() { //wybiera miejsce gdzie b�dzie prawid�owa odp
		correctPlace = (int) (Math.random() *4) + 1;
		Toast.makeText(getApplicationContext(), String.valueOf(correctPlace), Toast.LENGTH_LONG).show();

	}
}
